// Update with your config settings.

module.exports = {
  /**test: {
    client: 'pg',
    connection: 'postgres://lucid_lore:qweqwe@localhost/lucid_lore_test',
    migrations: {
      directory: __dirname + '/db/migrations'
    },
    seeds: {
      directory: __dirname + '/db/seeds/'           /// TODO separate test seeds???
    }
  },**/

  development: {
    client: 'pg',
    connection: 'postgres://lucid_lore:qweqwe@localhost/lucid_lore',
    migrations: {
      directory: __dirname + '/db/migrations',
      tableName: '__knex_migrations',
    },
    seeds: {
      directory: __dirname + '/db/seeds/',
    }
  },

  /**staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }**/

};
