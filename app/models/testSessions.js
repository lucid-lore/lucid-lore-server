const knex = require('../knex');
const BaseQueries = require('./BaseQueries');
const TestSessionsTable = require('./tables/testSessions');
const UsersTable = require('./tables/users');
const TestsTable = require('./tables/tests');
const TestGroupsTable = require('./tables/testGroups');
const { getObjectAllowedFields } = require('../utils/base');


class TestSessionsQueries extends BaseQueries {
    static get TABLE() { return TestSessionsTable.TABLE; }
    static get ID() { return TestSessionsTable.ID; }

    static createQueryWithJoins() {
        return this.createQuery()
            .leftJoin(UsersTable.TABLE, UsersTable.ID, TestSessionsTable.OWNER_ID)
            .leftJoin(TestsTable.TABLE, TestsTable.ID, TestSessionsTable.TEST_ID)
            .leftJoin(TestGroupsTable.TABLE, TestGroupsTable.ID, TestSessionsTable.TEST_GROUP_ID)
    }

    static selectFromQueryWithJoins(query) {
        return query.select({
            id: TestSessionsTable.ID,
            name: TestSessionsTable.NAME,
            status: TestSessionsTable.STATUS,
            duration: TestSessionsTable.DURATION,

            ownerId: TestSessionsTable.OWNER_ID,
            ownerLogin: UsersTable.LOGIN,
            ownerFio: UsersTable.FIO,

            testId: TestSessionsTable.TEST_ID,
            testName: TestsTable.NAME,

            testGroupId: TestSessionsTable.TEST_GROUP_ID,
            testGroupName: TestGroupsTable.NAME,
        });
    }

    static mapItems(items) {
        return items.map(_item => {
            const item = getObjectAllowedFields(_item, [
                'id', 'name', 'status', 'duration', 'ownerId', 'testId', 'testGroupId'
            ]);

            return Object.assign(item, {
                _owner: {
                    id: _item.ownerId,
                    fio: _item.ownerFio,
                    login: _item.ownerLogin,
                },

                _test: {
                    id: _item.testId,
                    name: _item.testName,
                },

                _testGroup: {
                    id: _item.testGroupId,
                    name: _item.testGroupName,
                },
            });
        });
    }

    ////////////////////////////////////////////////////////////////////////////

    static patchQueryWithSearch(query, params) {
        const { search } = params;
        if (!search) return query;

        query.where(queryBuilder => {
            super.patchQueryWithSearch(queryBuilder, params);

            queryBuilder.orWhere(TestSessionsTable.NAME, 'ilike', `%${search}%`);
        });

        return query;
    }

    ////////////////////////////////////////////////////////////////////////////

    static checkDataRestrictions(data) {
        if (data.status && !TestSessionsTable.ALLOWED_STATUSES.includes(data.status))
            throw new Error(`Wrong 'status' value ${data.status}`);
    }

    static createItem(data) {
        this.checkDataRestrictions(data);

        return super.createItem(data);
    }

    static updateItem(id, data) {
        this.checkDataRestrictions(data);

        return super.updateItem(id, data);
    }
}

////////////////////////////////////////////////////////////////////////////////

const createItem = data => TestSessionsQueries.createItem(data);

const getItem = id => TestSessionsQueries.getItem(id);

const updateItem = (id, data) => TestSessionsQueries.updateItem(id, data);

const dropItem = id => TestSessionsQueries.dropItem(id);

////////////////////////////////////////////////////////////////////////////////

const getList = (params) => TestSessionsQueries.getList(params);

const getShortItems = (params) => TestSessionsQueries.getShortItems(params);

////////////////////////////////////////////////////////////////////////////////

const getActualByGroupIds = (testGroupIds) =>
    knex(TestSessionsTable.TABLE)
        .select()
        .whereIn(TestSessionsTable.TEST_GROUP_ID, testGroupIds)
        .whereIn(TestSessionsTable.STATUS, [ TestSessionsTable.STATUS_PREPARING, TestSessionsTable.STATUS_ACTIVE ]);

const getActivatedByTestId = (testId) =>
    knex(TestSessionsTable.TABLE)
        .select()
        .where({ testId })
        .whereIn(TestSessionsTable.STATUS, [
            TestSessionsTable.STATUS_PREPARING,
            TestSessionsTable.STATUS_ACTIVE,
            TestSessionsTable.STATUS_FINISHED
        ]);

const getUnFinishedTestSessions = () =>
    knex(TestSessionsTable.TABLE)
        .select()
        .where(TestSessionsTable.STATUS, TestSessionsTable.STATUS_ACTIVE)
        .where(TestSessionsTable.ACTIVE_BEFORE, '<=', new Date);



module.exports = {
    createItem,
    getItem,
    updateItem,
    dropItem,

    getList,
    getShortItems,

    getActualByGroupIds,

    getActivatedByTestId,
    getUnFinishedTestSessions
};
