const knex = require('../knex');
const BaseQueries = require('./BaseQueries');
const TestGroupUsersTable = require('./tables/testGroupUsers');


const getIdsByTestGroupId = (testGroupId) =>
    knex(TestGroupUsersTable.TABLE)
        .select([ TestGroupUsersTable.USER_ID ])
        .where(TestGroupUsersTable.TEST_GROUP_ID, testGroupId)
            .then(rows => rows.map(r => r.userId));

const addUserToTestGroup = (testGroupId, userId) =>
    knex(TestGroupUsersTable.TABLE)
        .insert({ testGroupId, userId });

const dropUserFromGroup = (testGroupId, userId) =>
    knex(TestGroupUsersTable.TABLE)
        .where({ testGroupId, userId })
        .del();


const getGroupIdsByUserId = (userId) =>
    knex(TestGroupUsersTable.TABLE)
        .select([ TestGroupUsersTable.TEST_GROUP_ID ])
        .where(TestGroupUsersTable.USER_ID, userId)
            .then(rows => rows.map(r => r.testGroupId));


module.exports = {
    getIdsByTestGroupId,

    addUserToTestGroup,
    dropUserFromGroup,

    getGroupIdsByUserId,
};
