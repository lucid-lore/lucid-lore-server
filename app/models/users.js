const knex = require('../knex');
const BaseQueries = require('./BaseQueries');
const UsersTable = require('./tables/users');
const SchoolsTable = require('./tables/schools');
const ClassesTable = require('./tables/classes');
const { getFields } = require('./base');
const { getObjectAllowedFields } = require('../utils/base');


const getFullByLogin = login => knex(UsersTable.TABLE)
    .select().where({ login }).first();



const SANITIZE_FIELDS = [
    UsersTable.ID,
    UsersTable.LOGIN,
    UsersTable.DATAS,
    UsersTable.ROLE,
    UsersTable.FIO,
    UsersTable.EMAIL,
    UsersTable.PHONE,
    UsersTable.SCHOOL_ID,
    UsersTable.CLASS_ID,
];

const sanitizeEntity = user => getObjectAllowedFields(user, getFields(SANITIZE_FIELDS));

const getSanitizedById = id => knex(UsersTable.TABLE)
    .select(SANITIZE_FIELDS).where({ id }).first();

////////////////////////////////////////////////////////////////////////////////

class UsersQueries extends BaseQueries {
    static get TABLE() { return UsersTable.TABLE; }
    static get ID() { return UsersTable.ID; }

    static createQueryWithJoins() {
        return this.createQuery()
            .leftJoin(SchoolsTable.TABLE, SchoolsTable.ID, UsersTable.SCHOOL_ID)
            .leftJoin(ClassesTable.TABLE, ClassesTable.ID, UsersTable.CLASS_ID);
    }

    static selectFromQuery(query) {
        return query.select(SANITIZE_FIELDS);
    }

    static selectFromQueryWithJoins(query) {
        return query.select({
            id: UsersTable.ID,
            login: UsersTable.LOGIN,
            fio: UsersTable.FIO,
            email: UsersTable.EMAIL,
            phone: UsersTable.PHONE,

            schoolId: UsersTable.SCHOOL_ID,
            schoolName: SchoolsTable.NAME,

            classId: UsersTable.CLASS_ID,
            classLevel: ClassesTable.LEVEL,
            classLetter: ClassesTable.LETTER,
            className: ClassesTable.NAME,
        });
    }

    static mapItems(items) {
        return items.map(_item => {
            const item = getObjectAllowedFields(_item, [ 'id', 'login', 'fio', 'email', 'phone' ]);

            return Object.assign(item, {
                _school: {
                    id: _item.schoolId,
                    name: _item.schoolName,
                },

                _class: {
                    id: _item.classId,
                    level: _item.classLevel,
                    letter: _item.classLetter,
                    name: _item.className,
                },
            });
        });
    }


    static patchQueryWithFilters(query, params) {
        const { role, schoolId, classId, ids } = params;

        if (role) {
            query.where(UsersTable.ROLE, role);
        }
        if (schoolId) {
            query.where(UsersTable.SCHOOL_ID, schoolId);
        }
        if (classId) {
            query.where(UsersTable.CLASS_ID, classId);
        }
        if (ids) {
            query.whereIn(UsersTable.ID, ids);
        }

        return query;
    }

    static patchQueryWithSearch(query, params) {
        const { search } = params;
        if (!search) return query;

        query.where(queryBuilder => {
            super.patchQueryWithSearch(queryBuilder, params);

            queryBuilder.orWhere(UsersTable.LOGIN, 'ilike', `%${search}%`);
            queryBuilder.orWhere(UsersTable.FIO, 'ilike', `%${search}%`);
            queryBuilder.orWhere(UsersTable.EMAIL, 'ilike', `%${search}%`);
        });

        return query;
    }
}

////////////////////////////////////////////////////////////////////////////////

const createItem = (data, role) => knex(UsersTable.TABLE).insert({ ...data, role });

const getItem = (id, role) => knex(UsersTable.TABLE).select(SANITIZE_FIELDS).where({ id, role }).first();

const updateItem = (id, data, role) => knex(UsersTable.TABLE).where({ id, role }).update(data);

const dropItem = (id, role) => knex(UsersTable.TABLE).where({ id, role }).del();

////////////////////////////////////////////////////////////////////////////////

const getList = (params) => {
    if (params.sortBy === 'school') {
        params.sortBy = UsersTable.SCHOOL_ID;
    }
    if (params.sortBy === 'class') {
        params.sortBy = UsersTable.CLASS_ID;
    }

    return UsersQueries.getList(params);
};

const getShortItems = (params) => UsersQueries.getShortItems(params);


module.exports = {
    getFullByLogin,
    sanitizeEntity,
    getSanitizedById,

    createItem,
    getItem,
    updateItem,
    dropItem,

    getList,
    getShortItems,
};
