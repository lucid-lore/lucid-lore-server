const BaseQueries = require('./BaseQueries');
const DisciplinesTable = require('./tables/disciplines');


class DisciplinesQueries extends BaseQueries {
    static get TABLE() { return DisciplinesTable.TABLE; }
    static get ID() { return DisciplinesTable.ID; }

    static patchQueryWithSearch(query, params) {
        const { search } = params;
        if (!search) return query;

        query.where(queryBuilder => {
            super.patchQueryWithSearch(queryBuilder, params);

            queryBuilder.orWhere(DisciplinesTable.NAME, 'ilike', `%${search}%`);
        });

        return query;
    }
}

////////////////////////////////////////////////////////////////////////////////

const createItem = data => DisciplinesQueries.createItem(data);

const getItem = id => DisciplinesQueries.getItem(id);

const updateItem = (id, data) => DisciplinesQueries.updateItem(id, data);

const dropItem = id => DisciplinesQueries.dropItem(id);

////////////////////////////////////////////////////////////////////////////////

const getList = (params) => DisciplinesQueries.getList(params);

const getShortItems = (params) => DisciplinesQueries.getShortItems(params);


module.exports = {
    createItem,
    getItem,
    updateItem,
    dropItem,

    getList,
    getShortItems
};
