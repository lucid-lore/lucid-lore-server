const knex = require('../knex');
const TestAnswersTable = require('./tables/testAnswers');


const create = (data) =>
    knex(TestAnswersTable.TABLE)
        .insert(data);


const getAnswersByTestSessionId = (testSessionId) =>
    knex(TestAnswersTable.TABLE)
        .select()
        .where({ testSessionId })
        .orderBy(TestAnswersTable.ID, 'asc');


module.exports = {
    create,
    getAnswersByTestSessionId
};
