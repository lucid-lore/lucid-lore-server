const BaseQueries = require('./BaseQueries');
const TestGroupsTable = require('./tables/testGroups');
const SchoolsTable = require('./tables/schools');
const UsersTable = require('./tables/users');
const { getObjectAllowedFields } = require('../utils/base');


class TestGroupsQueries extends BaseQueries {
    static get TABLE() { return TestGroupsTable.TABLE; }
    static get ID() { return TestGroupsTable.ID; }

    static createQueryWithJoins() {
        return this.createQuery()
            .leftJoin(SchoolsTable.TABLE, SchoolsTable.ID, TestGroupsTable.SCHOOL_ID)
            .leftJoin(UsersTable.TABLE, UsersTable.ID, TestGroupsTable.OWNER_ID);
    }

    static selectFromQueryWithJoins(query) {
        return query.select({
            id: TestGroupsTable.ID,
            name: TestGroupsTable.NAME,

            schoolId: TestGroupsTable.SCHOOL_ID,
            schoolName: SchoolsTable.NAME,

            ownerId: TestGroupsTable.OWNER_ID,
            ownerLogin: UsersTable.LOGIN,
            ownerFio: UsersTable.FIO,
        });
    }

    static mapItems(items) {
        return items.map(_item => {
            const item = getObjectAllowedFields(_item, [ 'id', 'name', 'schoolId' ]);

            return Object.assign(item, {
                _school: {
                    id: _item.schoolId,
                    name: _item.schoolName,
                },

                _owner: {
                    id: _item.ownerId,
                    fio: _item.ownerFio,
                    login: _item.ownerLogin,
                }
            });
        });
    }

    ////////////////////////////////////////////////////////////////////////////

    static patchQueryWithFilters(query, params) {
        const { schoolId, ownerId } = params;

        if (schoolId) {
            query.where(TestGroupsTable.SCHOOL_ID, schoolId);
        }
        if (ownerId) {
            query.where(TestGroupsTable.OWNER_ID, ownerId);
        }

        return query;
    }

    static patchQueryWithSearch(query, params) {
        const { search } = params;
        if (!search) return query;

        query.where(queryBuilder => {
            super.patchQueryWithSearch(queryBuilder, params);

            queryBuilder.orWhere(TestGroupsTable.NAME, 'ilike', `%${search}%`);
        });

        return query;
    }
}

////////////////////////////////////////////////////////////////////////////////

const createItem = data => TestGroupsQueries.createItem(data);

const getItem = id => TestGroupsQueries.getItem(id);

const updateItem = (id, data) => TestGroupsQueries.updateItem(id, data);

const dropItem = id => TestGroupsQueries.dropItem(id);

////////////////////////////////////////////////////////////////////////////////

const getList = (params) => TestGroupsQueries.getList(params);

const getShortItems = (params) => TestGroupsQueries.getShortItems(params);


module.exports = {
    createItem,
    getItem,
    updateItem,
    dropItem,

    getList,
    getShortItems
};
