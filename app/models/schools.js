const BaseQueries = require('./BaseQueries');
const SchoolsTable = require('./tables/schools');


class SchoolsQueries extends BaseQueries {
    static get TABLE() { return SchoolsTable.TABLE; }
    static get ID() { return SchoolsTable.ID; }

    static patchQueryWithFilters(query, params) {
        const { schoolId } = params;

        if (schoolId) {
            query.where(SchoolsTable.ID, schoolId);
        }

        return query;
    }


    static patchQueryWithSearch(query, params) {
        const { search } = params;
        if (!search) return query;

        query.where(queryBuilder => {
            super.patchQueryWithSearch(queryBuilder, params);

            queryBuilder.orWhere(SchoolsTable.NAME, 'ilike', `%${search}%`);
        });

        return query;
    }
}

////////////////////////////////////////////////////////////////////////////////

const createItem = data => SchoolsQueries.createItem(data);

const getItem = id => SchoolsQueries.getItem(id);

const updateItem = (id, data) => SchoolsQueries.updateItem(id, data);

const dropItem = id => SchoolsQueries.dropItem(id);

////////////////////////////////////////////////////////////////////////////////

const getList = (params) => SchoolsQueries.getList(params);

const getShortItems = (params) => SchoolsQueries.getShortItems(params);


module.exports = {
    createItem,
    getItem,
    updateItem,
    dropItem,

    getList,
    getShortItems
};
