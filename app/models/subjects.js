const knex = require('../knex');
const BaseQueries = require('./BaseQueries');
const SubjectsTable = require('./tables/subjects');


class SubjectsQueries extends BaseQueries {
    static get TABLE() { return SubjectsTable.TABLE; }
    static get ID() { return SubjectsTable.ID; }

    static patchQueryWithFilters(query, params) {
        const { disciplineId, parentId } = params;

        if (disciplineId) {
            query.where(SubjectsTable.DISCIPLINE_ID, disciplineId);
        }
        if (parentId || parentId === null) {
            query.where(SubjectsTable.PARENT_ID, parentId);
        }

        return query;
    }

    static patchQueryWithSearch(query, params) {
        const { search } = params;
        if (!search) return query;

        query.where(queryBuilder => {
            super.patchQueryWithSearch(queryBuilder, params);

            queryBuilder.orWhere(SubjectsTable.NAME, 'ilike', `%${search}%`);
        });

        return query;
    }
}

////////////////////////////////////////////////////////////////////////////////

const createItem = data => SubjectsQueries.createItem(data);

const getItem = id => SubjectsQueries.getItem(id);

const updateItem = (id, data) => SubjectsQueries.updateItem(id, data);

const dropItem = id => SubjectsQueries.dropItem(id);

////////////////////////////////////////////////////////////////////////////////

const getList = (params) => SubjectsQueries.getList(params);

const getShortItems = (params) => SubjectsQueries.getShortItems(params);

////////////////////////////////////////////////////////////////////////////////

const getMaxOrder = async (disciplineId, parentId) => {
    const item = await knex(SubjectsTable.TABLE)
        .select([ SubjectsTable.ORDER ])
        .where({ disciplineId, parentId })
        .orderBy(SubjectsTable.ORDER, 'desc')
        .first();

    return item ? item.order : 0;
};

const getPreviousItem = ({ disciplineId, parentId, order }) => {
    return knex(SubjectsTable.TABLE)
        .select()
        .where({ disciplineId, parentId })
        .where(SubjectsTable.ORDER, '<', order)
        .orderBy(SubjectsTable.ORDER, 'desc')
        .first();
};

const getNextItem = ({ disciplineId, parentId, order }) => {
    return knex(SubjectsTable.TABLE)
        .select()
        .where({ disciplineId, parentId })
        .where(SubjectsTable.ORDER, '>', order)
        .orderBy(SubjectsTable.ORDER, 'asc')
        .first();
};



module.exports = {
    createItem,
    getItem,
    updateItem,
    dropItem,

    getList,
    getShortItems,

    getMaxOrder,
    getPreviousItem,
    getNextItem,
};
