const BaseTable = require('./BaseTable');


class TestQuestionsTable extends BaseTable {
    static get TABLE() { return 'test_questions' }

    static get ID() { return this.column('id') }
    static get TEST_ID() { return this.column('test_id') }
    static get TYPE() { return this.column('type') }
    static get ORDER() { return this.column('order') }
    static get TEXT() { return this.column('text') }
    static get ANSWERS() { return this.column('answers') }
    static get RIGHT_ANSWER() { return this.column('right_answer') }

    static get TYPE_OPTIONS() { return 'options' }
    static get TYPE_INPUT() { return 'input' }

    static get ALLOWED_TYPES() {
        return [ this.TYPE_OPTIONS, this.TYPE_INPUT ];
    }
}


module.exports = TestQuestionsTable;
