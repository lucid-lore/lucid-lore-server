const BaseTable = require('./BaseTable');


class TestGroupsTable extends BaseTable {
    static get TABLE() { return 'test_groups' }

    static get ID() { return this.column('id') }
    static get NAME() { return this.column('name') }
    static get SCHOOL_ID() { return this.column('school_id') }
    static get OWNER_ID() { return this.column('owner_id') }
}


module.exports = TestGroupsTable;
