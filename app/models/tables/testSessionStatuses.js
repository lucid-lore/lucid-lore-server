const BaseTable = require('./BaseTable');


class TestSessionStatusesTable extends BaseTable {
    static get TABLE() { return 'test_session_statuses' }

    static get ID() { return this.column('id') }
    static get USER_ID() { return this.column('user_id') }
    static get TEST_SESSION_ID() { return this.column('test_session_id') }

    static get FINISHED() { return this.column('finished') }
    static get QUESTION_NUMBER() { return this.column('question_number') }
    static get BLURRED_AT() { return this.column('blurred_at') }
    static get BLURRED_DURATION() { return this.column('blurred_duration') }

}


module.exports = TestSessionStatusesTable;
