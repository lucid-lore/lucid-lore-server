const BaseTable = require('./BaseTable');


class SchoolsTable extends BaseTable {
    static get TABLE() { return 'schools' }

    static get ID() { return this.column('id') }
    static get NAME() { return this.column('name') }
}


module.exports = SchoolsTable;