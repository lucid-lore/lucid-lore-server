const BaseTable = require('./BaseTable');


class TestsTable extends BaseTable {
    static get TABLE() { return 'tests' }

    static get ID() { return this.column('id') }
    static get NAME() { return this.column('name') }
    static get DISCIPLINE_ID() { return this.column('discipline_id') }
    static get OWNER_ID() { return this.column('owner_id') }
    static get DATAS() { return this.column('datas') }
}


module.exports = TestsTable;
