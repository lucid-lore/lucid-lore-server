const BaseTable = require('./BaseTable');


class TestAnswersTable extends BaseTable {
    static get TABLE() { return 'test_answers' }

    static get ID() { return this.column('id') }
    static get USER_ID() { return this.column('user_id') }
    static get TEST_SESSION_ID() { return this.column('test_session_id') }
    static get TEST_QUESTION_ID() { return this.column('test_question_id') }
    static get ANSWER() { return this.column('answer') }
    static get CREATED_AT() { return this.column('created_at') }
}


module.exports = TestAnswersTable;
