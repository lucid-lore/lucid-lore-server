const BaseTable = require('./BaseTable');


class TestSessionsTable extends BaseTable {
    static get TABLE() { return 'test_sessions' }

    static get ID() { return this.column('id') }
    static get OWNER_ID() { return this.column('owner_id') }
    static get TEST_ID() { return this.column('test_id') }
    static get TEST_GROUP_ID() { return this.column('test_group_id') }
    static get NAME() { return this.column('name') }
    static get DURATION() { return this.column('duration') }
    static get PREPARING_AT() { return this.column('preparing_at') }
    static get STARTED_AT() { return this.column('started_at') }
    static get STATUS() { return this.column('status') }
    static get ACTIVE_BEFORE() { return this.column('active_before') }

    static get STATUS_DRAFT() { return 'draft' }
    static get STATUS_PLANNED() { return 'planned' }
    static get STATUS_PREPARING() { return 'preparing' }
    static get STATUS_ACTIVE() { return 'active' }
    static get STATUS_FINISHED() { return 'finished' }

    static get ALLOWED_STATUSES() {
        return [
            this.STATUS_DRAFT,
            this.STATUS_PLANNED,
            this.STATUS_PREPARING,
            this.STATUS_ACTIVE,
            this.STATUS_FINISHED
        ];
    }
}


module.exports = TestSessionsTable;
