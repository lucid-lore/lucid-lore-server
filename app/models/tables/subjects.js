const BaseTable = require('./BaseTable');


class SubjectsTable extends BaseTable {
    static get TABLE() { return 'subjects' }

    static get ID() { return this.column('id') }
    static get NAME() { return this.column('name') }
    static get DISCIPLINE_ID() { return this.column('discipline_id') }
    static get PARENT_ID() { return this.column('parent_id') }
    static get ORDER() { return this.column('order') }
}


module.exports = SubjectsTable;
