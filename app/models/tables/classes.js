const BaseTable = require('./BaseTable');


class ClassesTable extends BaseTable {
    static get TABLE() { return 'classes' }

    static get ID() { return this.column('id') }
    static get SCHOOL_ID() { return this.column('school_id') }
    static get LEVEL() { return this.column('level') }
    static get LETTER() { return this.column('letter') }
    static get NAME() { return this.column('name') }
}


module.exports = ClassesTable;