const BaseTable = require('./BaseTable');


class TestGroupUsersTable extends BaseTable {
    static get TABLE() { return 'test_group_users' }

    static get TEST_GROUP_ID() { return this.column('test_group_id') }
    static get USER_ID() { return this.column('user_id') }
}


module.exports = TestGroupUsersTable;
