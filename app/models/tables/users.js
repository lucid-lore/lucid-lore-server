const BaseTable = require('./BaseTable');


class UsersTable extends BaseTable {
    static get TABLE() { return 'users' }

    static get ID() { return this.column('id') }
    static get LOGIN() { return this.column('login') }
    static get DATAS() { return this.column('datas') }
    static get ROLE() { return this.column('role') }

    static get SCHOOL_ID() { return this.column('school_id') }
    static get CLASS_ID() { return this.column('class_id') }

    static get FIO() { return this.column('fio') }
    static get EMAIL() { return this.column('email') }
    static get PHONE() { return this.column('phone') }
}


module.exports = UsersTable;