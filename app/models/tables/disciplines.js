const BaseTable = require('./BaseTable');


class DisciplinesTable extends BaseTable {
    static get TABLE() { return 'disciplines' }

    static get ID() { return this.column('id') }
    static get NAME() { return this.column('name') }
}


module.exports = DisciplinesTable;