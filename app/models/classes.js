const knex = require('../knex');
const BaseQueries = require('./BaseQueries');
const ClassesTable = require('./tables/classes');
const SchoolsTable = require('./tables/schools');
const { getObjectAllowedFields } = require('../utils/base');

////////////////////////////////////////////////////////////////////////////////

class ClassesQueries extends BaseQueries {
    static get TABLE() { return ClassesTable.TABLE; }
    static get ID() { return ClassesTable.ID; }

    static createQueryWithJoins() {
        return this.createQuery()
            .leftJoin(SchoolsTable.TABLE, SchoolsTable.ID, ClassesTable.SCHOOL_ID);
    }

    static selectFromQueryWithJoins(query) {
        return query.select({
            id: ClassesTable.ID,

            schoolId: ClassesTable.SCHOOL_ID,
            schoolName: SchoolsTable.NAME,

            level: ClassesTable.LEVEL,
            letter: ClassesTable.LETTER,
            name: ClassesTable.NAME,
        });
    }

    static mapItems(items) {
        return items.map(_item => {
            const item = getObjectAllowedFields(_item, [ 'id', 'level', 'letter', 'name' ]);

            return Object.assign(item, {
                _school: {
                    id: _item.schoolId,
                    name: _item.schoolName,
                },
            });
        });
    }


    static patchQueryWithFilters(query, params) {
        const { schoolId } = params;

        if (schoolId) {
            query.where(ClassesTable.SCHOOL_ID, schoolId);
        }

        return query;
    }

    static patchQueryWithSearch(query, params) {
        const { search } = params;
        if (!search) return query;

        query.where(queryBuilder => {
            super.patchQueryWithSearch(queryBuilder, params);

            queryBuilder.orWhere(ClassesTable.NAME, 'ilike', `%${search}%`);

            queryBuilder.orWhere(
                knex.raw(ClassesTable.LEVEL + '||' + ClassesTable.LETTER),
                'ilike',
                `%${search}%`
            );
        });

        return query;
    }
}

////////////////////////////////////////////////////////////////////////////////

const createItem = data => ClassesQueries.createItem(data);

const getItem = id => ClassesQueries.getItem(id);

const updateItem = (id, data) => ClassesQueries.updateItem(id, data);

const dropItem = id => ClassesQueries.dropItem(id);

////////////////////////////////////////////////////////////////////////////////

const getList = (params) => {
    if (params.sortBy === 'school') {
        params.sortBy = ClassesTable.SCHOOL_ID;
    }

    return ClassesQueries.getList(params);
};

const getShortItems = (params) => ClassesQueries.getShortItems(params);


module.exports = {
    createItem,
    getItem,
    updateItem,
    dropItem,

    getList,
    getShortItems
};
