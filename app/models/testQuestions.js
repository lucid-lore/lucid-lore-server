const knex = require('../knex');
const TestQuestionsTable = require('./tables/testQuestions');


////////////////////////////////////////////////////////////////////////////////

const checkDataRestrictions = data => {
    if (data.type && !TestQuestionsTable.ALLOWED_TYPES.includes(data.type))
        throw new Error(`Wrong 'type' value ${data.type}`);
};

const createItem = data => {
    checkDataRestrictions(data);

    return knex(TestQuestionsTable.TABLE)
        .insert(data)
        .returning('*')
        .then(rows => rows && rows[0]);
};

const getItem = id =>
    knex(TestQuestionsTable.TABLE)
        .where(TestQuestionsTable.ID, id)
        .first();

const updateItem = (id, data) => {
    checkDataRestrictions(data);

    return knex(TestQuestionsTable.TABLE)
        .where(TestQuestionsTable.ID, id)
        .update(data);
};

const dropItem = id =>
    knex(TestQuestionsTable.TABLE)
        .where(TestQuestionsTable.ID, id)
        .del();

////////////////////////////////////////////////////////////////////////////////

const getTestItems = testId =>
    knex(TestQuestionsTable.TABLE)
        .select()
        .where({ testId })
        .orderBy(TestQuestionsTable.ORDER, 'asc');

const getTestItemsForPupil = testId =>
    knex(TestQuestionsTable.TABLE)
        .select([
            TestQuestionsTable.ID,
            TestQuestionsTable.ORDER,
            TestQuestionsTable.TYPE,
            TestQuestionsTable.TEXT,
            TestQuestionsTable.ANSWERS
        ])
        .where({ testId })
        .orderBy(TestQuestionsTable.ORDER, 'asc');

const getMaxOrder = async (testId) => {
    const item = await knex(TestQuestionsTable.TABLE)
        .select([ TestQuestionsTable.ORDER ])
        .where({ testId })
        .orderBy(TestQuestionsTable.ORDER, 'desc')
        .first();

    return item ? item.order : 0;
};

const getPreviousItem = ({ testId, order }) => {
    return knex(TestQuestionsTable.TABLE)
        .select()
        .where({ testId })
        .where(TestQuestionsTable.ORDER, '<', order)
        .orderBy(TestQuestionsTable.ORDER, 'desc')
        .first();
};

const getNextItem = ({ testId, order }) => {
    return knex(TestQuestionsTable.TABLE)
        .select()
        .where({ testId })
        .where(TestQuestionsTable.ORDER, '>', order)
        .orderBy(TestQuestionsTable.ORDER, 'asc')
        .first();
};

const decrementOrderWhichMoreThan = (testId, order) =>
    knex(TestQuestionsTable.TABLE)
        .where({ testId })
        .andWhere('order', '>', order)
        .decrement('order');


module.exports = {
    createItem,
    getItem,
    updateItem,
    dropItem,

    getTestItems,
    getTestItemsForPupil,

    getMaxOrder,
    getPreviousItem,
    getNextItem,

    decrementOrderWhichMoreThan,
};
