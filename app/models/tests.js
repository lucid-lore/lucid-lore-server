const BaseQueries = require('./BaseQueries');
const TestsTable = require('./tables/tests');
const UsersTable = require('./tables/users');
const DisciplinesTable = require('./tables/disciplines');
const { getObjectAllowedFields } = require('../utils/base');


class TestsQueries extends BaseQueries {
    static get TABLE() { return TestsTable.TABLE; }
    static get ID() { return TestsTable.ID; }

    static createQueryWithJoins() {
        return this.createQuery()
            .leftJoin(UsersTable.TABLE, UsersTable.ID, TestsTable.OWNER_ID)
            .leftJoin(DisciplinesTable.TABLE, DisciplinesTable.ID, TestsTable.DISCIPLINE_ID);
    }

    static selectFromQueryWithJoins(query) {
        return query.select({
            id: TestsTable.ID,
            name: TestsTable.NAME,

            ownerId: TestsTable.OWNER_ID,
            ownerLogin: UsersTable.LOGIN,
            ownerFio: UsersTable.FIO,

            disciplineId: TestsTable.DISCIPLINE_ID,
            disciplineName: DisciplinesTable.NAME,
        });
    }

    static mapItems(items) {
        return items.map(_item => {
            const item = getObjectAllowedFields(_item, [ 'id', 'name', 'disciplineId' ]);

            return Object.assign(item, {
                _discipline: {
                    id: _item.disciplineId,
                    name: _item.disciplineName,
                },

                _owner: {
                    id: _item.ownerId,
                    fio: _item.ownerFio,
                    login: _item.ownerLogin,
                }
            });
        });
    }

    ////////////////////////////////////////////////////////////////////////////

    static patchQueryWithFilters(query, params) {
        const { disciplineId, ownerId } = params;

        if (disciplineId) {
            query.where(TestsTable.DISCIPLINE_ID, disciplineId);
        }
        if (ownerId) {
            query.where(TestsTable.OWNER_ID, ownerId);
        }

        return query;
    }

    static patchQueryWithSearch(query, params) {
        const { search } = params;
        if (!search) return query;

        query.where(queryBuilder => {
            super.patchQueryWithSearch(queryBuilder, params);

            queryBuilder.orWhere(TestsTable.NAME, 'ilike', `%${search}%`);
        });

        return query;
    }
}

////////////////////////////////////////////////////////////////////////////////

const createItem = data => TestsQueries.createItem(data);

const getItem = id => TestsQueries.getItem(id);

const updateItem = (id, data) => TestsQueries.updateItem(id, data);

const dropItem = id => TestsQueries.dropItem(id);

////////////////////////////////////////////////////////////////////////////////

const getList = (params) => TestsQueries.getList(params);

const getShortItems = (params) => TestsQueries.getShortItems(params);


module.exports = {
    createItem,
    getItem,
    updateItem,
    dropItem,

    getList,
    getShortItems
};
