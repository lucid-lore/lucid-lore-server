///// FIXME DROP cause useless

const dbUtils = require('./db');
const logger = require('./logger');

module.exports = function(pgPool) {
    const exec = async (text, values) => {
        logger.debug({ text, values });                             //// FIXME   [object Object]

        try {
            return await pgPool.query(text, values);
        } catch(err) {
            logger.error(err);
            logger.info({ text, values });

            throw err;
        }
    };

    const selectRaw = (text, values) =>
        exec(text, values)
            .then(res => res.rows);

    const select = (text, values) =>
        selectRaw(text, values)
            .then(rows => rows.map(dbUtils.convertKeysFromRaw));

    const selectFirstRaw = (text, values) =>
        selectRaw(text, values)
            .then(rows => rows[0]);

    const selectFirst = (text, values) =>
        selectFirstRaw(text, values)
            .then(row => row && dbUtils.convertKeysFromRaw(row));


    return {
        pool: pgPool,
        exec,
        select,
        selectFirst,
        selectRaw,
        selectFirstRaw,
    };
};
