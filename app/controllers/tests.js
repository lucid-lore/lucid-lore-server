const testsService = require('../services/tests');
const { wrapAsync } = require('../utils/base');
const { getBaseListParamsFromQuery } = require('../utils/list');


const getItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    const data = await testsService.getItem(applicationQuery, id);

    res.send(data);
});

const getList = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;
    const params = getBaseListParamsFromQuery(query, [ 'disciplineId' ]);

    const data = await testsService.getList(applicationQuery, params);

    res.send(data);
});

const getShortItems = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;
    const params = getBaseListParamsFromQuery(query, [ 'disciplineId' ]);

    const data = await testsService.getShortItems(applicationQuery, params);

    res.send(data);
});

////////////////////////////////////////////////////////////////////////////////

const createItem = wrapAsync(async (req, res) => {
    const { applicationQuery, body } = req;

    await testsService.createItem(applicationQuery, body);

    res.sendStatus(200);
});

const updateItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id }, body } = req;

    await testsService.updateItem(applicationQuery, id, body);

    res.sendStatus(200);
});


const dropItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await testsService.dropItem(applicationQuery, id);

    res.sendStatus(200);
});

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const createTestQuestion = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { testId } } = req;

    const question = await testsService.createTestQuestion(applicationQuery, testId);

    res.send(question);
});

const getTestQuestions = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { testId } } = req;

    const questions = await testsService.getTestQuestions(applicationQuery, testId);

    res.send(questions);
});

const saveTestQuestion = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id }, body } = req;

    await testsService.saveTestQuestion(applicationQuery, id, body);

    res.sendStatus(200);
});

const moveTestQuestionUp = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await testsService.moveTestQuestionUp(applicationQuery, id);

    res.sendStatus(200);
});

const moveTestQuestionDown = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await testsService.moveTestQuestionDown(applicationQuery, id);

    res.sendStatus(200);
});

const dropTestQuestion = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await testsService.dropTestQuestion(applicationQuery, id);

    res.sendStatus(200);
});


////////////////////////////////////////////////////////////////////////////////

module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    updateItem,
    dropItem,

    createTestQuestion,
    getTestQuestions,
    saveTestQuestion,
    moveTestQuestionUp,
    moveTestQuestionDown,
    dropTestQuestion,
};
