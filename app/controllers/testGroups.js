const testGroupsService = require('../services/testGroups');
const { wrapAsync } = require('../utils/base');
const { getBaseListParamsFromQuery } = require('../utils/list');


const getItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    const data = await testGroupsService.getItem(applicationQuery, id);

    res.send(data);
});

const getList = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;
    const params = getBaseListParamsFromQuery(query, [ 'schoolId' ]);

    const data = await testGroupsService.getList(applicationQuery, params);

    res.send(data);
});

const getShortItems = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;
    const params = getBaseListParamsFromQuery(query, [ 'schoolId' ]);

    const data = await testGroupsService.getShortItems(applicationQuery, params);

    res.send(data);
});

////////////////////////////////////////////////////////////////////////////

const getTestGroupUsersList = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    const data = await testGroupsService.getTestGroupUsersList(applicationQuery, id);

    res.send(data.items);
});


const addUserToTestGroup = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id, userId } } = req;

    await testGroupsService.addUserToTestGroup(applicationQuery, id, userId);

    res.sendStatus(200);
});

const dropUserFromTestGroup = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id, userId } } = req;

    await testGroupsService.dropUserFromTestGroup(applicationQuery, id, userId);

    res.sendStatus(200);
});

////////////////////////////////////////////////////////////////////////////////

const createItem = wrapAsync(async (req, res) => {
    const { applicationQuery, body } = req;
    await testGroupsService.createItem(applicationQuery, body);

    res.sendStatus(200);
});

const updateItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id }, body } = req;

    await testGroupsService.updateItem(applicationQuery, id, body);

    res.sendStatus(200);
});


const dropItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await testGroupsService.dropItem(applicationQuery, id);

    res.sendStatus(200);
});


module.exports = {
    getItem,
    getList,
    getShortItems,

    getTestGroupUsersList,
    addUserToTestGroup,
    dropUserFromTestGroup,

    createItem,
    updateItem,
    dropItem,
};
