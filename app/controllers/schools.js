const schoolsService = require('../services/schools');
const { wrapAsync } = require('../utils/base');
const { getBaseListParamsFromQuery } = require('../utils/list');


const getItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    const data = await schoolsService.getItem(applicationQuery, id);

    res.send(data);
});

const getUserSchool = wrapAsync(async (req, res) => {
    const { applicationQuery } = req;

    const data = await schoolsService.getItem(
        applicationQuery,
        applicationQuery.getUser().getSchoolId()
    );

    res.send(data);
});


const getList = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;
    const params = getBaseListParamsFromQuery(query);

    const data = await schoolsService.getList(applicationQuery, params);

    res.send(data);
});

const getShortItems = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;
    const params = getBaseListParamsFromQuery(query);

    const data = await schoolsService.getShortItems(applicationQuery, params);

    res.send(data);
});

////////////////////////////////////////////////////////////////////////////////

const createItem = wrapAsync(async (req, res) => {
    const { applicationQuery, body } = req;
    await schoolsService.createItem(applicationQuery, body);

    res.sendStatus(200);
});

const updateItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id }, body } = req;

    await schoolsService.updateItem(applicationQuery, id, body);

    res.sendStatus(200);
});


const dropItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await schoolsService.dropItem(applicationQuery, id);

    res.sendStatus(200);
});


module.exports = {
    getItem,
    getList,
    getShortItems,

    getUserSchool,

    createItem,
    updateItem,
    dropItem,
};
