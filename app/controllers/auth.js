const authService = require('../services/auth');
const { badRequestError } = require('../utils/errors');
const rolesPermissions = require('../constants/rolesPermissions');

const login = async (req, res, next) => {

    try {
        const login = req.body.login.trim().toLowerCase();
        const password = req.body.password.trim();

        if (!login) throw badRequestError('Expected login in body');
        if (!password) throw badRequestError('Expected password in body');

        const { user, sid } = await authService.login(login, password);

        res.json({
            sid,
            user,
            permissions: rolesPermissions[user.role] || []
        });
    } catch(err) {
        next(err);
    }
};

const session = (req, res) => {
    const { applicationQuery } = req;

    res.send({
        sid: applicationQuery.getSession().getSid(),
        user: applicationQuery.getUser().getRawUser(),
        permissions: applicationQuery.getUser().getPermissions()
    });
};

const logout = async (req, res) => {
    const { applicationQuery } = req;

    await authService.logout(applicationQuery.getSession().getSid());

    res.sendStatus(200);
};


module.exports = {
    login,
    session,
    logout
};
