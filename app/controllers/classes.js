const classesService = require('../services/classes');
const { wrapAsync } = require('../utils/base');
const { getBaseListParamsFromQuery } = require('../utils/list');


const getItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    const data = await classesService.getItem(applicationQuery, id);

    res.send(data);
});


const getList = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;

    const params = getBaseListParamsFromQuery(query, [ 'schoolId' ]);
    const data = await classesService.getList(applicationQuery, params);

    res.send(data);
});

const getShortItems = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;

    const params = getBaseListParamsFromQuery(query, [ 'schoolId' ]);
    const data = await classesService.getShortItems(applicationQuery, params);

    res.send(data);
});

////////////////////////////////////////////////////////////////////////////////

const createItem = wrapAsync(async (req, res) => {
    const { applicationQuery, body } = req;

    await classesService.createItem(applicationQuery, body);

    res.sendStatus(200);
});

const updateItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id }, body } = req;

    await classesService.updateItem(applicationQuery, id, body);

    res.sendStatus(200);
});


const dropItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await classesService.dropItem(applicationQuery, id);

    res.sendStatus(200);
});


module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    updateItem,
    dropItem,
};
