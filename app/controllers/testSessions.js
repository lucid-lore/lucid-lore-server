const testSessionsService = require('../services/testSessions');
const { wrapAsync } = require('../utils/base');
const { getBaseListParamsFromQuery } = require('../utils/list');


const getItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    const data = await testSessionsService.getItem(applicationQuery, id);

    res.send(data);
});

const getList = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;
    const params = getBaseListParamsFromQuery(query);

    const data = await testSessionsService.getList(applicationQuery, params);

    res.send(data);
});

////////////////////////////////////////////////////////////////////////////////

const createItem = wrapAsync(async (req, res) => {
    const { applicationQuery, body } = req;
    await testSessionsService.createItem(applicationQuery, body);

    res.sendStatus(200);
});

const updateItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id }, body } = req;

    await testSessionsService.updateItem(applicationQuery, id, body);

    res.sendStatus(200);
});


const dropItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await testSessionsService.dropItem(applicationQuery, id);

    res.sendStatus(200);
});


const getTestSessionInfo = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    const data = await testSessionsService.getTestSessionInfo(applicationQuery, id);

    res.send(data);
});

const setTestSessionStatus = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id }, body: { status } } = req;

    await testSessionsService.setTestSessionStatus(applicationQuery, id, status);

    res.sendStatus(200);
});


module.exports = {
    getItem,
    getList,

    createItem,
    updateItem,
    dropItem,

    getTestSessionInfo,
    setTestSessionStatus,
};
