const subjectsService = require('../services/subjects');
const { wrapAsync } = require('../utils/base');
const { getBaseListParamsFromQuery } = require('../utils/list');


const getItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    const data = await subjectsService.getItem(applicationQuery, id);

    res.send(data);
});

const getList = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;
    const params = Object.assign(
        { parentId: null },
        getBaseListParamsFromQuery(query, [ 'disciplineId', 'parentId' ])
    );

    const data = await subjectsService.getList(applicationQuery, params);

    res.send(data);
});

const getShortItems = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;
    const params = Object.assign(
        { parentId: null },
        getBaseListParamsFromQuery(query, [ 'disciplineId', 'parentId' ])
    );

    const data = await subjectsService.getShortItems(applicationQuery, params);

    res.send(data);
});

////////////////////////////////////////////////////////////////////////////////

const createItem = wrapAsync(async (req, res) => {
    const { applicationQuery, body } = req;
    await subjectsService.createItem(applicationQuery, body);

    res.sendStatus(200);
});


const updateItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id }, body } = req;

    await subjectsService.updateItem(applicationQuery, id, body);

    res.sendStatus(200);
});


const moveItemUp = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await subjectsService.moveItemUp(applicationQuery, id);

    res.sendStatus(200);
});

const moveItemDown = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await subjectsService.moveItemDown(applicationQuery, id);

    res.sendStatus(200);
});


const dropItem = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await subjectsService.dropItem(applicationQuery, id);

    res.sendStatus(200);
});


module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    updateItem,
    dropItem,

    moveItemUp,
    moveItemDown,
};
