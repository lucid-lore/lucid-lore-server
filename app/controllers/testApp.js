const testAppService = require('../services/testApp');
const { wrapAsync } = require('../utils/base');


const getActualTestSessions = wrapAsync(async (req, res) => {
    const { applicationQuery } = req;

    const data = await testAppService.getActualTestSessions(applicationQuery);

    res.send(data);
});

const getActiveTestSession = wrapAsync(async (req, res) => {
    const { applicationQuery } = req;

    const data = await testAppService.getUserActiveTestSession(applicationQuery);

    res.send(data);
});

const setActiveTestSession = wrapAsync(async (req, res) => {
    const { applicationQuery, params: { testSessionId } } = req;

    await testAppService.setUserActiveTestSession(applicationQuery, testSessionId);

    res.sendStatus(200);
});

const postAnswer = wrapAsync(async (req, res) => {
    const { applicationQuery, body } = req;

    await testAppService.postAnswer(applicationQuery, body);

    res.sendStatus(200);
});

const dropActiveTestSession = wrapAsync(async (req, res) => {
    const { applicationQuery } = req;

    await testAppService.dropUserActiveTestSession(applicationQuery);

    res.sendStatus(200);
});


module.exports = {
    getActualTestSessions,
    getActiveTestSession,
    setActiveTestSession,
    dropActiveTestSession,

    postAnswer,
};
