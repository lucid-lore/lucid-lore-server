const usersService = require('../services/users');
const { wrapAsync } = require('../utils/base');
const { getBaseListParamsFromQuery } = require('../utils/list');


const getItem = role => wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    const data = await usersService.getItem(applicationQuery, id, role);

    res.send(data);
});


const getList = role => wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;

    const params = getBaseListParamsFromQuery(query, [ 'schoolId', 'classId' ]);
    const data = await usersService.getList(applicationQuery, params, role);

    res.send(data);
});

const getShortItems = wrapAsync(async (req, res) => {
    const { applicationQuery, query } = req;

    const params = getBaseListParamsFromQuery(query, [ 'schoolId' ]);
    const data = await usersService.getShortItems(applicationQuery, params);

    res.send(data);
});

////////////////////////////////////////////////////////////////////////////////

const createItem = role => wrapAsync(async (req, res) => {
    const { applicationQuery, body } = req;

    await usersService.createItem(applicationQuery, body, role);

    res.sendStatus(200);
});

const updateItem = role => wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id }, body } = req;

    await usersService.editItem(applicationQuery, id, body, role);

    res.sendStatus(200);
});


const dropItem = role => wrapAsync(async (req, res) => {
    const { applicationQuery, params: { id } } = req;

    await usersService.dropItem(applicationQuery, id, role);

    res.sendStatus(200);
});


module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    updateItem,
    dropItem
};
