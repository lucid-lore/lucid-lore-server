const { addMinutes } = require('date-fns');
const knex = require('../knex');
const ACTIONS = require('../constants/actions');
const DB_ERROR_CODES = require('../constants/dbErrorCodes');

const testSessionsModel = require('../models/testSessions');
const TestSessionsTable = require('../models/tables/testSessions');
const testsModel = require('../models/tests');
const testQuestionsModel = require('../models/testQuestions');
const testGroupUsersModel = require('../models/testGroupUsers');
const testSessionStatusesModel = require('../models/testSessionStatuses');
const testAnswersModel = require('../models/testAnswers');
const usersModel = require('../models/users');

const socketService = require('../socket.io');
const baseUtils = require('../utils/base');
const { trim } = require('../utils/normalize');
const { badRequestError, forbiddenError, notFoundError } = require('../utils/errors');

const ownerHelper = require('./helpers/owner');


const getItem = (applicationQuery, id) => {
    return testSessionsModel.getItem(id);
};

const getList = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        params.ownerId = applicationUser.getId();
    }

    return testSessionsModel.getList(params);
};

////////////////////////////////////////////////////////////////////////////////

const normalizeAndValidateData = (_data = {}) => {
    const { testId, testGroupId, duration } = _data;
    const name = trim(_data.name);

    if (!name) throw badRequestError('Missing name', 'Отсутствует название');
    if (!testId) throw badRequestError('Выберите Тест');
    if (!testGroupId) throw badRequestError('Выберите Группу тестирования');
    if (!duration) throw badRequestError('Укажите продолжительность');
    if (duration <= 0) throw badRequestError('Продолжительность должна быть положительна');

    return { name, testId, testGroupId, duration };
};


const createItem = async (applicationQuery, _data) => {
    const applicationUser = applicationQuery.getUser();

    const data = normalizeAndValidateData(_data);

    if (!_data.ownerId || !applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        data.ownerId = applicationUser.getId();
    }

    return await testSessionsModel.createItem(data);
};

const updateItem = async (applicationQuery, id, _data) => {
    const data = normalizeAndValidateData(_data);

    const item = await testSessionsModel.getItem(id);
    if (item.status !== TestSessionsTable.STATUS_DRAFT) throw forbiddenError('Restrict to edit activated testSession');

    return await testSessionsModel.updateItem(id, data);
};


const dropItem = async (applicationQuery, id) => {
    const item = testSessionsModel.getItem(id);
    if (item.status !== TestSessionsTable.STATUS_DRAFT) throw forbiddenError('Restrict to drop activated testSession');

    try {
        return await testSessionsModel.dropItem(id);
    } catch (err) {
        if (err.code === DB_ERROR_CODES.FOREIGN_KEY_VIOLATION) {
            throw forbiddenError(err.detail);
        }

        throw err;
    }
};

////////////////////////////////////////////////////////////////////////////////

const getTestSessionInfo = async (applicationQuery, id) => {
    const testSession = await testSessionsModel.getItem(id);
    ownerHelper.checkOwnerAccess(applicationQuery, testSession);

    const test = await testsModel.getItem(testSession.testId);
    const questions = await testQuestionsModel.getTestItems(testSession.testId);

    const testGroupUserIds = await testGroupUsersModel.getIdsByTestGroupId(testSession.testGroupId);

    const { items: users } = await usersModel.getList({
        role: 'pupil',                              /// TODO constant
        ids: testGroupUserIds,
        sortBy: 'id',
        limit: -1
    });

    const testSessionStatuses = await testSessionStatusesModel.getBySessionId(id);
    const userStatuses = baseUtils.accumulateToObject(
        testSessionStatuses,
        row => ({ [row.userId]: row })
    );

    const online = baseUtils.accumulateToObject(
        socketService.getOnlineUsersByIds( users.map(u => u.id) ),
        id => ({ [id]: id })
    );

    ////////////////////////////////////////////////////////
    const questionsById = baseUtils.accumulateToObject(
        questions,
        row => ({ [row.id]: row })
    );

    const answers = await testAnswersModel.getAnswersByTestSessionId(id);

    const userAnswers = {};
    answers.forEach(answerItem => {
        const { userId, answer, testQuestionId } = answerItem;

        if (!userAnswers[userId]) userAnswers[userId] = [];

        userAnswers[userId].push(
            Object.assign(answerItem, {
                right: answer === questionsById[testQuestionId].rightAnswer
            })
        );
    });

    Object.keys(userAnswers).forEach(userId => {
        userStatuses[userId].currentPercentage =
            userAnswers[ userId ].filter(a => !!a.right).length
            / userAnswers[ userId ].length
            * 100;

        userStatuses[userId].rightPercentage =
            userAnswers[ userId ].filter(a => !!a.right).length
            / questions.length
            * 100;
    });

    return Object.assign(testSession, { test, questions, users, userStatuses, userAnswers, online });
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const _setTestSessionStatusToDraft = async (applicationQuery, testSession) => {
    if (testSession.status !== TestSessionsTable.STATUS_PREPARING)
        throw forbiddenError('Expected testSession with status preparing');

    await knex.transaction(async trx => {
        await Promise.all([
            testSessionStatusesModel
                .dropByTestSessionId(testSession.id)
                .transacting(trx),

            testSessionsModel
                .updateItem(testSession.id, {
                    status: TestSessionsTable.STATUS_DRAFT,
                    preparingAt: null,
                })
                .transacting(trx),
        ]);
    });

    /// Notify ///
    const testGroupUserIds = await testGroupUsersModel.getIdsByTestGroupId(testSession.testGroupId);

    socketService.forceUsersToReloadTestApp( testGroupUserIds );
    socketService.forceUsersToReloadTestAppActualSessions( testGroupUserIds );
};

const _setTestSessionStatusToPreparing = async (applicationQuery, testSession) => {
    if (testSession.status !== TestSessionsTable.STATUS_DRAFT)
        throw forbiddenError('Expected testSession with status draft');

    await testSessionsModel.updateItem(testSession.id, {
        status: TestSessionsTable.STATUS_PREPARING,
        preparingAt: new Date,
    });

    /// Notify ///
    const testGroupUserIds = await testGroupUsersModel.getIdsByTestGroupId(testSession.testGroupId);

    socketService.forceUsersToReloadTestAppActualSessions( testGroupUserIds );
};

const _setTestSessionStatusToActive = async (applicationQuery, testSession) => {
    if (testSession.status !== TestSessionsTable.STATUS_PREPARING)
        throw forbiddenError('Expected testSession with status preparing');

    const startedAt = new Date;
    await knex.transaction(async trx => {
        await Promise.all([
            testSessionStatusesModel
                .updateWholeTestSession(testSession.id, {
                    startedAt: new Date
                })
                .transacting(trx),

            testSessionsModel
                .updateItem(testSession.id, {
                    status: TestSessionsTable.STATUS_ACTIVE,
                    startedAt,
                    activeBefore: addMinutes(startedAt, testSession.duration),
                })
                .transacting(trx),
        ]);
    });

    /// Notify ///
    const testGroupUserIds = await testGroupUsersModel.getIdsByTestGroupId(testSession.testGroupId);

    socketService.forceUsersToReloadTestApp( testGroupUserIds );
};

const setTestSessionStatus = async (applicationQuery, id, status) => {
    const testSession = await testSessionsModel.getItem(id);
    if (!testSession) throw notFoundError();
    ownerHelper.checkOwnerAccess(applicationQuery, testSession);

    switch (status) {
        case TestSessionsTable.STATUS_DRAFT:
            await _setTestSessionStatusToDraft(applicationQuery, testSession);
            break;

        case TestSessionsTable.STATUS_PREPARING:
            await _setTestSessionStatusToPreparing(applicationQuery, testSession);
            break;

        case TestSessionsTable.STATUS_ACTIVE:
            await _setTestSessionStatusToActive(applicationQuery, testSession);
            break;

        default:
            throw new Error('Unexpected testSession status: ' + status);
    }
};


const finishTestSession = async (testSessionId) => {
    const testSession = await testSessionsModel.getItem(testSessionId);
    await testSessionsModel.updateItem(testSessionId, { status: TestSessionsTable.STATUS_FINISHED });       /// TODO transac

    await testSessionStatusesModel.finishTestSessionWhole(testSessionId);

    /// Notify ///
    const testGroupUserIds = await testGroupUsersModel.getIdsByTestGroupId(testSession.testGroupId);

    socketService.forceUsersToReloadTestApp( testGroupUserIds );
    socketService.forceUsersToReloadTestAppActualSessions( testGroupUserIds );
    socketService.forceTestSessionUpdate(testSessionId);
};


const checkUnFinishedTestSessions = async () => {
    const unFinishedTestSessions = await testSessionsModel.getUnFinishedTestSessions();
    unFinishedTestSessions.length
        && console.log('__checkUnFinishedTestSessions', unFinishedTestSessions.map(ts => ts.id));

    await Promise.all(
        unFinishedTestSessions.map(ts => finishTestSession(ts.id))
    );
};

module.exports = {
    getItem,
    getList,

    createItem,
    updateItem,
    dropItem,

    getTestSessionInfo,
    setTestSessionStatus,

    finishTestSession,
    checkUnFinishedTestSessions
};
