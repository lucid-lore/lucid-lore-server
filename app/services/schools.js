const ACTIONS = require('../constants/actions');
const DB_ERROR_CODES = require('../constants/dbErrorCodes');
const schoolsModel = require('../models/schools');
const { trim } = require('../utils/normalize');
const { badRequestError, forbiddenError } = require('../utils/errors');

const getItem = async (applicationQuery, id) => {
    const applicationUser = applicationQuery.getUser();

    const item = await schoolsModel.getItem(id);

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        const schoolId = applicationUser.getSchoolId();

        if (item.id !== schoolId) throw forbiddenError();
    }

    return item;
};

const getList = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        params.schoolId = applicationUser.getSchoolId();
    }

    return schoolsModel.getList(params);
};

const getShortItems = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        params.schoolId = applicationUser.getSchoolId();
    }

    return schoolsModel.getShortItems(params);
};

////////////////////////////////////////////////////////////////////////////////

const normalizeData = (data = {}) => {
    return {
        name: trim(data.name)
    }
};

const createItem = async (applicationQuery, data) => {
    const { name } = normalizeData(data);

    if (!name) throw badRequestError('Missing name', 'Отсутствует название');

    return await schoolsModel.createItem({ name });
};


const updateItem = async (applicationQuery, id, _data) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        const item = await schoolsModel.getItem(id);

        if (item.id !== applicationUser.getSchoolId()) throw forbiddenError();
    }

    const data = normalizeData(_data);
    if (!data.name) throw badRequestError('Missing name', 'Отсутствует название');

    return await schoolsModel.updateItem(id, data);
};


const dropItem = async (applicationQuery, id) => {
    try {
        return await schoolsModel.dropItem(id);
    } catch (err) {
        if (err.code === DB_ERROR_CODES.FOREIGN_KEY_VIOLATION) {
            throw forbiddenError(err.detail);
        }

        throw err;
    }
};


module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    updateItem,
    dropItem,
};
