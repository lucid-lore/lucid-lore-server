const ACTIONS = require('../constants/actions');
const DB_ERROR_CODES = require('../constants/dbErrorCodes');
const testGroupsModel = require('../models/testGroups');
const testGroupUsersModel = require('../models/testGroupUsers');
const usersModel = require('../models/users');

const { trim } = require('../utils/normalize');
const { badRequestError, forbiddenError } = require('../utils/errors');

const getItem = (applicationQuery, id) => {
    return testGroupsModel.getItem(id);
};

const getList = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        params.schoolId = applicationUser.getSchoolId();
    }
    if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        params.ownerId = applicationUser.getId();
    }

    return testGroupsModel.getList(params);
};

const getShortItems = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        params.schoolId = applicationUser.getSchoolId();
    }
    if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        params.ownerId = applicationUser.getId();
    }

    return testGroupsModel.getShortItems(params);
};

///////////////////////////////////////////////////////////////////////////////////

const getTestGroupUsersList = async (applicationQuery, id) => {
    const testGroup = await testGroupsModel.getItem(id);

    ///  TODO check & perms

    const testGroupUserIds = await testGroupUsersModel.getIdsByTestGroupId(testGroup.id);

    const users = await usersModel.getList({
        role: 'pupil',                              /// TODO constant
        ids: testGroupUserIds,
        sortBy: 'id',
        limit: -1
    });

    return users;
};

const addUserToTestGroup = async (applicationQuery, testGroupId, userId) => {
    ///  TODO checkPerms
    return await testGroupUsersModel.addUserToTestGroup(testGroupId, userId);
};

const dropUserFromTestGroup = async (applicationQuery, testGroupId, userId) => {
    ///  TODO checkPerms
    return await testGroupUsersModel.dropUserFromGroup(testGroupId, userId);
};

////////////////////////////////////////////////////////////////////////////////

const normalizeAndValidateData = (_data = {}) => {
    const { schoolId } = _data;
    const name = trim(_data.name);

    if (!name) throw badRequestError('Missing name', 'Отсутствует название');
    if (!schoolId) throw badRequestError('Missing school', 'Отсутствует школа');

    return { name, schoolId };
};


const createItem = async (applicationQuery, _data) => {
    const applicationUser = applicationQuery.getUser();

    const data = normalizeAndValidateData(_data);

    if (!_data.ownerId || !applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        data.ownerId = applicationUser.getId();
    }

    return await testGroupsModel.createItem(data);
};


const checkUserRestrictionForItem = async (applicationUser, id) => {
    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)
        || !applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)
    ) {
        const item = await testGroupsModel.getItem(id);
        if (!item) throw notFoundError();

        if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)
            && item.schoolId !== applicationUser.getSchoolId()) throw forbiddenError();

        if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)
            && item.ownerId !== applicationUser.getId()) throw forbiddenError();
    }
};

const updateItem = async (applicationQuery, id, _data) => {
    const applicationUser = applicationQuery.getUser();
    const data = normalizeAndValidateData(_data);

    await checkUserRestrictionForItem(applicationUser, id);

    return await testGroupsModel.updateItem(id, data);
};


const dropItem = async (applicationQuery, id) => {
    const applicationUser = applicationQuery.getUser();

    try {
        await checkUserRestrictionForItem(applicationUser, id);

        return await testGroupsModel.dropItem(id);
    } catch (err) {
        if (err.code === DB_ERROR_CODES.FOREIGN_KEY_VIOLATION) {
            throw forbiddenError(err.detail);
        }

        throw err;
    }
};


module.exports = {
    getItem,
    getList,
    getShortItems,

    getTestGroupUsersList,
    addUserToTestGroup,
    dropUserFromTestGroup,

    createItem,
    updateItem,
    dropItem,
};
