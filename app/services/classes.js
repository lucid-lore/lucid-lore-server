const ACTIONS = require('../constants/actions');
const classesModel = require('../models/classes');
const { trim } = require('../utils/normalize');
const { badRequestError, forbiddenError } = require('../utils/errors');


const getItem = async (applicationQuery, id) => {
    const applicationUser = applicationQuery.getUser();

    const item = await classesModel.getItem(id);

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        const schoolId = applicationUser.getSchoolId();

        if (item.schoolId !== schoolId) throw forbiddenError();
    }

    return item;
};

const getList = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        if (params.schoolId) {
            if (params.schoolId !== applicationUser.getSchoolId()) throw forbiddenError();
        } else {
            params.schoolId = applicationUser.getSchoolId();
        }
    }

    return classesModel.getList(params);
};

const getShortItems = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        if (params.schoolId) {
            if (params.schoolId !== applicationUser.getSchoolId()) throw forbiddenError();
        } else {
            params.schoolId = applicationUser.getSchoolId();
        }
    }

    return classesModel.getShortItems(params);
};

////////////////////////////////////////////////////////////////////////////////

const normalizeData = (_data = {}) => {
    const data = {
        schoolId: _data.schoolId,
        level: _data.level && parseInt(_data.level),
        letter: trim(_data.letter),
        name: trim(_data.name),
    };

    if (!data.schoolId) throw badRequestError('Нехватает школы');
    if (!data.name && (!data.level || !data.letter)) throw badRequestError('Нехватает номера\\буквы или названия');

    return data;
};

const createItem = async (applicationQuery, _data) => {
    const applicationUser = applicationQuery.getUser();
    const data = normalizeData(_data);

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        if (data.schoolId !== applicationUser.getSchoolId()) throw forbiddenError();
    }

    return await classesModel.createItem(data);
};


const updateItem = async (applicationQuery, id, _data) => {
    const applicationUser = applicationQuery.getUser();
    const data = normalizeData(_data);

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        if (data.schoolId !== applicationUser.getSchoolId()) throw forbiddenError();

        const item = await classesModel.getItem(id);
        if (item.schoolId !== applicationUser.getSchoolId()) throw forbiddenError();
    }

    return await classesModel.updateItem(id, data);
};


const dropItem = async (applicationQuery, id) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        const item = await classesModel.getItem(id);
        if (item.schoolId !== applicationUser.getSchoolId()) throw forbiddenError();
    }

    return await classesModel.dropItem(id);
};


module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    updateItem,
    dropItem,
};
