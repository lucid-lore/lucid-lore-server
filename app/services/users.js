const ACTIONS = require('../constants/actions');
const DB_ERROR_CODES = require('../constants/dbErrorCodes');
const usersModel = require('../models/users');
const classesModel = require('../models/classes');
const authUtils = require('../utils/auth');
const { badRequestError, forbiddenError, notFoundError } = require('../utils/errors');


const getItem = async (applicationQuery, id, role) => {
    const applicationUser = applicationQuery.getUser();

    const item = await usersModel.getItem(id, role);

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS) && item.schoolId) {
        const schoolId = applicationUser.getSchoolId();

        if (item.schoolId !== schoolId) throw forbiddenError();
    }

    return item;
};


const getList = (applicationQuery, params, role) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        if (params.schoolId) {
            if (params.schoolId !== applicationUser.getSchoolId()) throw forbiddenError();
        } else {
            params.schoolId = applicationUser.getSchoolId();
        }
    }

    return usersModel.getList(
        Object.assign(params, { role })
    );
};

const getShortItems = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        params.schoolId = applicationUser.getSchoolId();
    }

    return usersModel.getShortItems(params);
};

////////////////////////////////////////////////////////////////////////////////

const prepareData = async (applicationQuery, _data, role, isCreation = false) => {
    const applicationUser = applicationQuery.getUser();
    const { login, password, fio, email, phone, schoolId, classId } = _data;

    if (!login) throw badRequestError('Missing login', 'Отсутствует логин');
    const data = {
        login: login.trim().toLowerCase(),
    };

    if (isCreation && !password) throw badRequestError('Missing password', 'Отсутствует пароль');
    if (password) {
        const { hash, salt } = await authUtils.hashPromisified({ password });
        Object.assign(data, { hash, salt });
    }

    if (role !== 'admin') {
        if (!fio) throw badRequestError('Missing FIO', 'Отсутствует ФИО');
        ///if (!email) throw badRequestError('Missing email', 'Отсутствует email');
        ///if (!phone) throw badRequestError('Missing phone', 'Отсутствует телефон');
        Object.assign(data, { fio, email, phone });

        if (!schoolId) throw badRequestError('Missing school', 'Отсутствует школа');
        if ( !applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS) ) {
            if (schoolId !== applicationUser.getSchoolId()) throw forbiddenError('School restriction');
        }
        Object.assign(data, { schoolId });

        if (role === 'pupil') {
            if (!classId) throw badRequestError('Missing class', 'Отсутствует класс');

            const $class = await classesModel.getItem(classId);                             /// Super paranoid mode
            if (!$class || $class.schoolId !== schoolId) throw badRequestError('School\\Class collision');

            Object.assign(data, { classId });
        }
    }

    return data;
};

const createItem = async (applicationQuery, _data, role) => {
    const data = await prepareData(applicationQuery, _data, role, true);

    return await usersModel.createItem(data, role);
};


const checkUserRestrictionForItem = async (applicationUser, id, role) => {
    if (!applicationUser.checkPermission(ACTIONS.ANY_SCHOOL_ACCESS)) {
        const item = await usersModel.getItem(id, role);
        if (!item) throw notFoundError();
        if (item.schoolId !== applicationUser.getSchoolId()) throw forbiddenError();
    }
};

const editItem = async (applicationQuery, id, _data, role) => {
    const applicationUser = applicationQuery.getUser();
    const data = await prepareData(applicationQuery, _data, role);

    await checkUserRestrictionForItem(applicationUser, id, role);

    return await usersModel.updateItem(id, data, role);
};


const dropItem = async (applicationQuery, id, role) => {
    const applicationUser = applicationQuery.getUser();

    try {
        await checkUserRestrictionForItem(applicationUser, id, role);

        return await usersModel.dropItem(id, role);
    } catch (err) {
        if (err.code === DB_ERROR_CODES.FOREIGN_KEY_VIOLATION) {
            throw forbiddenError(err.detail);
        }

        throw err;
    }
};


module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    editItem,
    dropItem,
};