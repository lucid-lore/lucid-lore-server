const knex = require('../knex');
const DB_ERROR_CODES = require('../constants/dbErrorCodes');
const subjectsModel = require('../models/subjects');
const { trim } = require('../utils/normalize');
const { badRequestError, forbiddenError } = require('../utils/errors');

const getItem = (applicationQuery, id) => {
    return subjectsModel.getItem(id);
};

const getList = (applicationQuery, params) => {
    return subjectsModel.getList(params);
};

const getShortItems = (applicationQuery, params) => {
    return subjectsModel.getShortItems(params);
};

////////////////////////////////////////////////////////////////////////////////

const normalizeAndValidateData = (_data = {}) => {
    const name = trim(_data.name);

    if (!name) throw badRequestError('Missing name', 'Отсутствует название');
    if (!_data.disciplineId) throw badRequestError('Missing discipline', 'Отсутствует дисциплина');


    return {
        name,
        disciplineId: _data.disciplineId,
        parentId: _data.parentId || null,
        ///order: 0
    };
};


const createItem = async (applicationQuery, _data) => {
    const data = normalizeAndValidateData(_data);

    const order = await subjectsModel.getMaxOrder(data.disciplineId, data.parentId);

    return await subjectsModel.createItem(
        Object.assign(data, {
            order: order + 1
        })
    );
};

const updateItem = async (applicationQuery, id, _data) => {
    const data = normalizeAndValidateData(_data);

    return await subjectsModel.updateItem(id, data);
};


const moveItemUp = (applicationQuery, id) => {
    return knex.transaction(async trx => {
        const item = await subjectsModel.getItem(id)
            .transacting(trx);

        const previousItem = await subjectsModel.getPreviousItem(item)
            .transacting(trx);
        if (!previousItem) return;

        await Promise.all([
            subjectsModel.updateItem(item.id, { order: previousItem.order })
                .transacting(trx),
            subjectsModel.updateItem(previousItem.id, { order: item.order })
                .transacting(trx),
        ]);
    });
};

const moveItemDown = (applicationQuery, id) => {
    return knex.transaction(async trx => {
        const item = await subjectsModel.getItem(id)
            .transacting(trx);
        
        const nextItem = await subjectsModel.getNextItem(item)
            .transacting(trx);
        if (!nextItem) return;

        await Promise.all([
            subjectsModel.updateItem(item.id, { order: nextItem.order })
                .transacting(trx),
            subjectsModel.updateItem(nextItem.id, { order: item.order })
                .transacting(trx),
        ]);
    });
};


const dropItem = async (applicationQuery, id) => {
    try {
        return await subjectsModel.dropItem(id);
    } catch (err) {
        if (err.code === DB_ERROR_CODES.FOREIGN_KEY_VIOLATION) {
            throw forbiddenError(err.detail);
        }

        throw err;
    }
};


module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    updateItem,
    dropItem,

    moveItemUp,
    moveItemDown,
};
