const DB_ERROR_CODES = require('../constants/dbErrorCodes');
const disciplinesModel = require('../models/disciplines');
const { trim } = require('../utils/normalize');
const { badRequestError, forbiddenError } = require('../utils/errors');

const getItem = (applicationQuery, id) => {
    return disciplinesModel.getItem(id);
};

const getList = (applicationQuery, params) => {
    return disciplinesModel.getList(params);
};

const getShortItems = (applicationQuery, params) => {
    return disciplinesModel.getShortItems(params);
};

////////////////////////////////////////////////////////////////////////////////

const normalizeAndValidateData = (_data = {}) => {
    const name = trim(_data.name);

    if (!name) throw badRequestError('Missing name', 'Отсутствует название');

    return { name };
};


const createItem = async (applicationQuery, _data) => {
    const data = normalizeAndValidateData(_data);

    return await disciplinesModel.createItem(data);
};

const updateItem = async (applicationQuery, id, _data) => {
    const data = normalizeAndValidateData(_data);

    return await disciplinesModel.updateItem(id, data);
};


const dropItem = async (applicationQuery, id) => {
    try {
        return await disciplinesModel.dropItem(id);
    } catch (err) {
        if (err.code === DB_ERROR_CODES.FOREIGN_KEY_VIOLATION) {
            throw forbiddenError(err.detail);
        }

        throw err;
    }
};


module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    updateItem,
    dropItem,
};
