const knex = require('../knex');
const ACTIONS = require('../constants/actions');
const DB_ERROR_CODES = require('../constants/dbErrorCodes');
const testsModel = require('../models/tests');
const testQuestionsModel = require('../models/testQuestions');
const testSessionsModel = require('../models/testSessions');
const { trim } = require('../utils/normalize');
const { badRequestError, forbiddenError } = require('../utils/errors');

const getItem = (applicationQuery, id) => {
    return testsModel.getItem(id);
};

const getList = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        params.ownerId = applicationUser.getId();
    }

    return testsModel.getList(params);
};

const getShortItems = (applicationQuery, params) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        params.ownerId = applicationUser.getId();
    }

    return testsModel.getShortItems(params);
};

////////////////////////////////////////////////////////////////////////////////

const normalizeAndValidateData = (_data = {}, isUpdate = false) => {
    const { disciplineId } = _data;
    const name = trim(_data.name);

    if (!name) throw badRequestError('Missing name', 'Отсутствует название');
    if (!disciplineId) throw badRequestError('Missing discipline', 'Отсутствует дисциплина');
    const data = { name, disciplineId };

    if (isUpdate) {
        const { rates, recomendedDuration } = _data.datas;

        if (rates[5] > 100 || rates[4] > 100 || rates[3] > 100) throw badRequestError('Коэффициэнты оценок не могут быть больше 100%');
        if (rates[5] < 0 || rates[4] < 0 || rates[3] < 0) throw badRequestError('Коэффициэнты оценок не могут быть меньше 0%');
        if (!(rates[5] > rates[4] && rates[4] > rates[3])) throw badRequestError('Ошибка в коэффициэнтах оценок (5>4>3)');
        const datas = { rates };

        if (recomendedDuration) Object.assign(datas, { recomendedDuration });

        Object.assign(data, { datas });
    }

    return data;
};


const createItem = async (applicationQuery, _data) => {
    const applicationUser = applicationQuery.getUser();
    const data = normalizeAndValidateData(_data);

    if (!_data.ownerId || !applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        data.ownerId = applicationUser.getId();
    }

    Object.assign(data, {
        datas: {
            rates: {
                5: 80,
                4: 60,
                3: 40,
            }
        }
    });

    return await testsModel.createItem(data);
};

const updateItem = async (applicationQuery, id, _data) => {
    const applicationUser = applicationQuery.getUser();
    const data = normalizeAndValidateData(_data, true);

    const activeTestSessions = await testSessionsModel.getActivatedByTestId(id);
    if (activeTestSessions.length) throw forbiddenError('Restirct to edit activated test');

    if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        if (data.ownerId !== applicationUser.getId()) throw forbiddenError();

        const testItem = await testsModel.getItem(id);

        if (testItem.ownerId !== applicationUser.getId()) throw forbiddenError();
    }

    return await testsModel.updateItem(id, data);
};


const dropItem = async (applicationQuery, id) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        const testItem = await testsModel.getItem(testId);

        if (testItem.ownerId !== applicationUser.getId()) throw forbiddenError();
    }

    try {
        return await testsModel.dropItem(id);
    } catch (err) {
        if (err.code === DB_ERROR_CODES.FOREIGN_KEY_VIOLATION) {
            throw forbiddenError(err.detail);
        }

        throw err;
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const createTestQuestion = async (applicationQuery, testId) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        const testItem = await testsModel.getItem(testId);

        if (testItem.ownerId !== applicationUser.getId()) throw forbiddenError();
    }

    const maxOrder = await testQuestionsModel.getMaxOrder(testId);

    return await testQuestionsModel.createItem({
        testId,
        order: maxOrder + 1,
    })
};

const getTestQuestions = async (applicationQuery, testId) => {
    const applicationUser = applicationQuery.getUser();

    if (!applicationUser.checkPermission(ACTIONS.ANY_OWNER_ACCESS)) {
        const testItem = await testsModel.getItem(testId);

        if (testItem.ownerId !== applicationUser.getId()) throw forbiddenError();
    }

    return await testQuestionsModel.getTestItems(testId);
};

const saveTestQuestion = async (applicationQuery, id, data) => {
    const applicationUser = applicationQuery.getUser();
    //// TODO CHECKS!!!!

    const testQuestion = await testQuestionsModel.getItem(id);
    const activeTestSessions = await testSessionsModel.getActivatedByTestId(testQuestion.testId);
    if (activeTestSessions.length) throw forbiddenError('Restirct to edit activated test');

    await testQuestionsModel.updateItem(id, data);
};

const moveTestQuestionUp = async (applicationQuery, id) => {
    const applicationUser = applicationQuery.getUser();
    //// TODO CHECKS!!!!

    await knex.transaction(async trx => {
        const item = await testQuestionsModel.getItem(id)
            .transacting(trx);

        const activeTestSessions = await testSessionsModel.getActivatedByTestId(item.testId);
        if (activeTestSessions.length) throw forbiddenError('Restirct to edit activated test');

        const previousItem = await testQuestionsModel.getPreviousItem(item)
            .transacting(trx);
        if (!previousItem) return;

        await Promise.all([
            testQuestionsModel.updateItem(item.id, { order: previousItem.order })
                .transacting(trx),
            testQuestionsModel.updateItem(previousItem.id, { order: item.order })
                .transacting(trx),
        ]);
    });
};

const moveTestQuestionDown = async (applicationQuery, id) => {
    const applicationUser = applicationQuery.getUser();
    //// TODO CHECKS!!!!

    await knex.transaction(async trx => {
        const item = await testQuestionsModel.getItem(id)
            .transacting(trx);

        const activeTestSessions = await testSessionsModel.getActivatedByTestId(item.testId);
        if (activeTestSessions.length) throw forbiddenError('Restirct to edit activated test');

        const nextItem = await testQuestionsModel.getNextItem(item)
            .transacting(trx);
        if (!nextItem) return;

        await Promise.all([
            testQuestionsModel.updateItem(item.id, { order: nextItem.order })
                .transacting(trx),
            testQuestionsModel.updateItem(nextItem.id, { order: item.order })
                .transacting(trx),
        ]);
    });
};

const dropTestQuestion = async (applicationQuery, id) => {
    const applicationUser = applicationQuery.getUser();
    //// TODO CHECKS!!!!

    await knex.transaction(async trx => {
        const item = await testQuestionsModel.getItem(id)
            .transacting(trx);

        const activeTestSessions = await testSessionsModel.getActivatedByTestId(item.testId);
        if (activeTestSessions.length) throw forbiddenError('Restirct to edit activated test');

        await testQuestionsModel.dropItem(id)
            .transacting(trx);

        await testQuestionsModel.decrementOrderWhichMoreThan(item.testId, item.order)
            .transacting(trx);
    });
};


module.exports = {
    getItem,
    getList,
    getShortItems,

    createItem,
    updateItem,
    dropItem,

    createTestQuestion,
    getTestQuestions,
    saveTestQuestion,
    moveTestQuestionUp,
    moveTestQuestionDown,
    dropTestQuestion,
};
