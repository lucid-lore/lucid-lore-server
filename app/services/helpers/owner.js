const ACTIONS = require('../../constants/actions');

/**
 *
 * @param {ApplicationQuery} applicationQuery
 * @param {Object} item
 * @returns {boolean}
 */
const haveOwnerAccess = (applicationQuery, item = {}) => {
    const applicationUser = applicationQuery.getUser();

    return applicationUser.havePermission(ACTIONS.ANY_OWNER_ACCESS)
        || item.ownerId === applicationUser.getId();
};

/**
 *
 * @param {ApplicationQuery} applicationQuery
 * @param {Object} item
 * @param {string=} forbiddenText
 */
const checkOwnerAccess = (applicationQuery, item, forbiddenText) => {
    if (!haveOwnerAccess(applicationQuery, item)) throw forbiddenError(forbiddenText);
};


module.exports = {
    haveOwnerAccess,
    checkOwnerAccess
};
