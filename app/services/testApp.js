const { differenceInSeconds } = require('date-fns');
const knex = require('../knex');
const testGroupUsersModel = require('../models/testGroupUsers');
const testSessionsModel = require('../models/testSessions');
const TestSessionsTable = require('../models/tables/testSessions');
const testSessionStatusesModel = require('../models/testSessionStatuses');
const testQuestionsModel = require('../models/testQuestions');
const testAnswersModel = require('../models/testAnswers');
const socketService = require('../socket.io');
const { badRequestError, forbiddenError, notFoundError } = require('../utils/errors');


const getActualTestSessions = async (applicationQuery) => {
    const applicationUser = applicationQuery.getUser();

    const testGroupIds = await testGroupUsersModel.getGroupIdsByUserId( applicationUser.getId() );

    let actualTestSessions = await testSessionsModel.getActualByGroupIds(testGroupIds);

    const finishedUserTestSessionStatuses = await testSessionStatusesModel.getFinishedUserSessions( applicationUser.getId() );
    const finishedUserTestSessionIds = finishedUserTestSessionStatuses.map(r => r.testSessionId);
    actualTestSessions = actualTestSessions.filter(i => !finishedUserTestSessionIds.includes(i.id));

    return actualTestSessions;
};


const getUserActiveTestSession = async (applicationQuery) => {
    const applicationUser = applicationQuery.getUser();

    const activeSession = await testSessionStatusesModel.getUserActiveTestSessionStatus( applicationUser.getId() );
    const data = activeSession;

    if (activeSession) {
        const testSession = await testSessionsModel.getItem( activeSession.testSessionId );
        Object.assign(data, { testSession });

        if (testSession.status === TestSessionsTable.STATUS_ACTIVE) {
            const testQuestions = await testQuestionsModel.getTestItemsForPupil(testSession.testId);
            Object.assign(data, { testQuestions });
        }
    }

    return data;
};


const JOINABLE_TEST_SESSION_STATUSES = [ TestSessionsTable.STATUS_PREPARING, TestSessionsTable.STATUS_ACTIVE ];

const setUserActiveTestSession = async (applicationQuery, testSessionId) => {
    const applicationUser = applicationQuery.getUser();

    const activeSession = await testSessionStatusesModel.getUserActiveTestSessionStatus( applicationUser.getId() );
    if (activeSession) throw forbiddenError('Already have active test session');

    const testSession = await testSessionsModel.getItem(testSessionId);
    if (!testSession) throw notFoundError();
    if (!JOINABLE_TEST_SESSION_STATUSES.includes(testSession.status)) {
        throw forbiddenError('TestSession inActive');
    }

    await testSessionStatusesModel.create(
        applicationUser.getId(),
        testSessionId,
        testSession.status === TestSessionsTable.STATUS_ACTIVE && { startedAt: new Date }
    );

    /// Notify ///
    socketService.forceTestSessionUpdate(testSessionId);
};

const dropUserActiveTestSession = async (applicationQuery) => {
    const applicationUser = applicationQuery.getUser();

    const activeSession = await testSessionStatusesModel.getUserActiveTestSessionStatus( applicationUser.getId() );
    if (!activeSession) return;
    const testSession = await testSessionsModel.getItem(activeSession.testSessionId);
    if ([ TestSessionsTable.STATUS_ACTIVE, TestSessionsTable.STATUS_FINISHED ].includes(testSession.status))
        throw forbiddenError('Cannot drop testSessionStatus for activated testSession');

    await testSessionStatusesModel.dropNotFinishedByUserId( applicationUser.getId() );

    /// Notify ///
    socketService.forceTestSessionUpdate(activeSession.testSessionId);
};


const postAnswer = async (applicationQuery, _data) => {
    const applicationUser = applicationQuery.getUser();

    if (!_data.questionId) throw badRequestError('Miss questionId');
    if (!_data.answer) throw badRequestError('Miss answer');
    if (!_data.answer.trim()) throw badRequestError('Пустой ответ');

    const activeSession = await testSessionStatusesModel.getUserActiveTestSessionStatus( applicationUser.getId() );
    if (!activeSession) throw forbiddenError('Missing active test session');
    const { testSessionId, id: testSessionStatusId } = activeSession;

    const testSession = await testSessionsModel.getItem( testSessionId );                 /// TODO CACHE
    /// TODO check testSession not Finished
    const testQuestions = await testQuestionsModel.getTestItemsForPupil(testSession.testId);

    const { questionId: testQuestionId } = _data;
    const answer = _data.answer.trim();

    const answerData = {
        userId: applicationUser.getId(),
        testSessionId,
        testQuestionId,
        answer
    };

    const testQuestion = testQuestions.find(q => q.id === testQuestionId);

    await knex.transaction(async trx => {
        await testAnswersModel.create(answerData)
            .transacting(trx);

        if (testQuestion.order === testQuestions.length) {
            await testSessionStatusesModel.updateItem(testSessionStatusId, {
                finished: true,
                finishedAt: new Date
            })
                .transacting(trx);
        } else {
            await testSessionStatusesModel.updateItem(testSessionStatusId, {
                questionNumber: testQuestion.order + 1
            })
                .transacting(trx);
        }
    });

    /// Notify ///
    socketService.forceTestSessionUpdate(testSessionId);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const userGoneOffline = async (userId) => {
    const activeSession = await testSessionStatusesModel.getUserActiveTestSessionStatus( userId );
    if (!activeSession || activeSession.offlineAt) return;

    await testSessionStatusesModel.updateItem(activeSession.id, {
        offlineAt: new Date
    });

    /// Notify ///
    socketService.forceTestSessionUpdate(activeSession.testSessionId);
};

const userCameOnline = async (userId) => {
    const activeSession = await testSessionStatusesModel.getUserActiveTestSessionStatus( userId );
    if (!activeSession || !activeSession.offlineAt) return;

    await testSessionStatusesModel.updateItem(activeSession.id, {
        offlineAt: null,
        offlineDuration: activeSession.offlineDuration + differenceInSeconds(new Date, activeSession.offlineAt)             /// FIXME to increase??
    });

    /// Notify ///
    socketService.forceTestSessionUpdate(activeSession.testSessionId);
};

////////////////////////////////////////////////////////////////////////////////

const userOnTestAppBlur = async (userId) => {
    const activeSession = await testSessionStatusesModel.getUserActiveTestSessionStatus( userId );
    if (!activeSession || activeSession.blurredAt) return;

    await testSessionStatusesModel.updateItem(activeSession.id, {
        blurredAt: new Date
    });

    /// Notify ///
    socketService.forceTestSessionUpdate(activeSession.testSessionId);
};

const userOnTestAppFocus = async (userId) => {
    const activeSession = await testSessionStatusesModel.getUserActiveTestSessionStatus( userId );
    if (!activeSession || !activeSession.blurredAt) return;

    await testSessionStatusesModel.updateItem(activeSession.id, {
        blurredAt: null,
        blurredDuration: activeSession.blurredDuration + differenceInSeconds(new Date, activeSession.blurredAt)             /// FIXME to increase??
    });

    /// Notify ///
    socketService.forceTestSessionUpdate(activeSession.testSessionId);
};

socketService.eventEmitter.on('userConnected', userCameOnline);
socketService.eventEmitter.on('userDisconnected', userGoneOffline);
socketService.eventEmitter.on('testAppBlur', userOnTestAppBlur);
socketService.eventEmitter.on('testAppFocus', userOnTestAppFocus);



module.exports = {
    getActualTestSessions,
    getUserActiveTestSession,
    setUserActiveTestSession,
    dropUserActiveTestSession,
    postAnswer,
};
