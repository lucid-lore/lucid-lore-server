const express = require('express');
const router = express.Router();
const logger = require('../utils/logger');
const ACTIONS = require('../constants/actions');

const authed = require('../middlewares/authed');
const checkPermission = require('../middlewares/checkPermission');

const authController = require('../controllers/auth');
const testAppController = require('../controllers/testApp');
const usersController = require('../controllers/users');
const schoolsController = require('../controllers/schools');
const classesController = require('../controllers/classes');
const disciplinesController = require('../controllers/disciplines');
const subjectsController = require('../controllers/subjects');
const testGroupsController = require('../controllers/testGroups');
const testsController = require('../controllers/tests');
const testSessionsController = require('../controllers/testSessions');
/// HERE Inject Controller Require ///

/// Session ///
router.post('/login', authController.login);
router.get('/session', authed, authController.session);
router.get('/school', authed, schoolsController.getUserSchool);
router.post('/logout', authed, authController.logout);

router.get('/testApp/sessions', authed, testAppController.getActualTestSessions);
router.get('/testApp/active', authed, testAppController.getActiveTestSession);
router.post('/testApp/active/:testSessionId', authed, testAppController.setActiveTestSession);
router.delete('/testApp/active', authed, testAppController.dropActiveTestSession);
router.post('/testApp/answer', authed, testAppController.postAnswer);


/// Users ///
router.get('/admin/admins', authed, checkPermission(ACTIONS.USERS_ADMINS_READ), usersController.getList('admin'));
router.post('/admin/admins', authed, checkPermission(ACTIONS.USERS_ADMINS_CREATE), usersController.createItem('admin'));
router.get('/admin/admins/:id', authed, checkPermission(ACTIONS.USERS_ADMINS_READ), usersController.getItem('admin'));
router.put('/admin/admins/:id', authed, checkPermission(ACTIONS.USERS_ADMINS_UPDATE), usersController.updateItem('admin'));
router.delete('/admin/admins/:id', authed, checkPermission(ACTIONS.USERS_ADMINS_DELETE), usersController.dropItem('admin'));

router.get('/admin/school_admins', authed, checkPermission(ACTIONS.USERS_SCHOOL_ADMINS_READ), usersController.getList('school_admin'));
router.post('/admin/school_admins', authed, checkPermission(ACTIONS.USERS_SCHOOL_ADMINS_CREATE), usersController.createItem('school_admin'));
router.get('/admin/school_admins/:id', authed, checkPermission(ACTIONS.USERS_SCHOOL_ADMINS_READ), usersController.getItem('school_admin'));
router.put('/admin/school_admins/:id', authed, checkPermission(ACTIONS.USERS_SCHOOL_ADMINS_UPDATE), usersController.updateItem('school_admin'));
router.delete('/admin/school_admins/:id', authed, checkPermission(ACTIONS.USERS_SCHOOL_ADMINS_DELETE), usersController.dropItem('school_admin'));

router.get('/admin/teachers', authed, checkPermission(ACTIONS.USERS_TEACHERS_READ), usersController.getList('teacher'));
router.post('/admin/teachers', authed, checkPermission(ACTIONS.USERS_TEACHERS_CREATE), usersController.createItem('teacher'));
router.get('/admin/teachers/:id', authed, checkPermission(ACTIONS.USERS_TEACHERS_READ), usersController.getItem('teacher'));
router.put('/admin/teachers/:id', authed, checkPermission(ACTIONS.USERS_TEACHERS_UPDATE), usersController.updateItem('teacher'));
router.delete('/admin/teachers/:id', authed, checkPermission(ACTIONS.USERS_TEACHERS_DELETE), usersController.dropItem('teacher'));

router.get('/admin/pupils', authed, checkPermission(ACTIONS.USERS_PUPILS_READ), usersController.getList('pupil'));
router.post('/admin/pupils', authed, checkPermission(ACTIONS.USERS_PUPILS_CREATE), usersController.createItem('pupil'));
router.get('/admin/pupils/:id', authed, checkPermission(ACTIONS.USERS_PUPILS_READ), usersController.getItem('pupil'));
router.put('/admin/pupils/:id', authed, checkPermission(ACTIONS.USERS_PUPILS_UPDATE), usersController.updateItem('pupil'));
router.delete('/admin/pupils/:id', authed, checkPermission(ACTIONS.USERS_PUPILS_DELETE), usersController.dropItem('pupil'));

router.get('/admin/users/short', authed, checkPermission(ACTIONS.USERS_READ), usersController.getShortItems);


/// Schools ///
router.get('/admin/schools', authed, checkPermission(ACTIONS.SCHOOLS_READ), schoolsController.getList);
router.get('/admin/schools/short', authed, checkPermission(ACTIONS.SCHOOLS_READ), schoolsController.getShortItems);
router.post('/admin/schools', authed, checkPermission(ACTIONS.SCHOOLS_CREATE), schoolsController.createItem);
router.get('/admin/schools/:id', authed, checkPermission(ACTIONS.SCHOOLS_READ), schoolsController.getItem);
router.put('/admin/schools/:id', authed, checkPermission(ACTIONS.SCHOOLS_UPDATE), schoolsController.updateItem);
router.delete('/admin/schools/:id', authed, checkPermission(ACTIONS.SCHOOLS_DELETE), schoolsController.dropItem);

/// Classes ///
router.get('/admin/classes', authed, checkPermission(ACTIONS.CLASSES_READ), classesController.getList);
router.get('/admin/classes/short', authed, checkPermission(ACTIONS.CLASSES_READ), classesController.getShortItems);
router.post('/admin/classes', authed, checkPermission(ACTIONS.CLASSES_CREATE), classesController.createItem);
router.get('/admin/classes/:id', authed, checkPermission(ACTIONS.CLASSES_READ), classesController.getItem);
router.put('/admin/classes/:id', authed, checkPermission(ACTIONS.CLASSES_UPDATE), classesController.updateItem);
router.delete('/admin/classes/:id', authed, checkPermission(ACTIONS.CLASSES_DELETE), classesController.dropItem);

/// Disciplines ///
router.get('/admin/disciplines', authed, checkPermission(ACTIONS.DISCIPLINES_READ), disciplinesController.getList);
router.get('/admin/disciplines/short', authed, checkPermission(ACTIONS.DISCIPLINES_READ), disciplinesController.getShortItems);
router.post('/admin/disciplines', authed, checkPermission(ACTIONS.DISCIPLINES_CREATE), disciplinesController.createItem);
router.get('/admin/disciplines/:id', authed, checkPermission(ACTIONS.DISCIPLINES_READ), disciplinesController.getItem);
router.put('/admin/disciplines/:id', authed, checkPermission(ACTIONS.DISCIPLINES_UPDATE), disciplinesController.updateItem);
router.delete('/admin/disciplines/:id', authed, checkPermission(ACTIONS.DISCIPLINES_DELETE), disciplinesController.dropItem);

/// Subjects ///
/**router.get('/admin/subjects', authed, checkPermission(ACTIONS.SUBJECTS_READ), subjectsController.getList);
router.get('/admin/subjects/short', authed, checkPermission(ACTIONS.SUBJECTS_READ), subjectsController.getShortItems);
router.post('/admin/subjects', authed, checkPermission(ACTIONS.SUBJECTS_CREATE), subjectsController.createItem);
router.get('/admin/subjects/:id', authed, checkPermission(ACTIONS.SUBJECTS_READ), subjectsController.getItem);
router.put('/admin/subjects/:id', authed, checkPermission(ACTIONS.SUBJECTS_UPDATE), subjectsController.updateItem);
router.put('/admin/subjects/:id/up', authed, checkPermission(ACTIONS.SUBJECTS_UPDATE), subjectsController.moveItemUp);
router.put('/admin/subjects/:id/down', authed, checkPermission(ACTIONS.SUBJECTS_UPDATE), subjectsController.moveItemDown);
router.delete('/admin/subjects/:id', authed, checkPermission(ACTIONS.SUBJECTS_DELETE), subjectsController.dropItem);**/

/// Test Groups ///
router.get('/admin/testGroups', authed, checkPermission(ACTIONS.TEST_GROUPS_READ), testGroupsController.getList);
router.get('/admin/testGroups/short', authed, checkPermission(ACTIONS.TEST_GROUPS_READ), testGroupsController.getShortItems);

router.get('/admin/testGroupUsers/:id', authed, checkPermission(ACTIONS.TEST_GROUPS_READ), testGroupsController.getTestGroupUsersList);
router.post('/admin/testGroupUsers/:id/:userId', authed, checkPermission(ACTIONS.TEST_GROUPS_READ), testGroupsController.addUserToTestGroup);
router.delete('/admin/testGroupUsers/:id/:userId', authed, checkPermission(ACTIONS.TEST_GROUPS_READ), testGroupsController.dropUserFromTestGroup);

router.post('/admin/testGroups', authed, checkPermission(ACTIONS.TEST_GROUPS_CREATE), testGroupsController.createItem);
router.get('/admin/testGroups/:id', authed, checkPermission(ACTIONS.TEST_GROUPS_READ), testGroupsController.getItem);
router.put('/admin/testGroups/:id', authed, checkPermission(ACTIONS.TEST_GROUPS_UPDATE), testGroupsController.updateItem);
router.delete('/admin/testGroups/:id', authed, checkPermission(ACTIONS.TEST_GROUPS_DELETE), testGroupsController.dropItem);

/// Tests ///
router.get('/admin/tests', authed, checkPermission(ACTIONS.TESTS_READ), testsController.getList);
router.get('/admin/tests/short', authed, checkPermission(ACTIONS.TESTS_READ), testsController.getShortItems);
router.post('/admin/tests', authed, checkPermission(ACTIONS.TESTS_CREATE), testsController.createItem);
router.get('/admin/tests/:id', authed, checkPermission(ACTIONS.TESTS_READ), testsController.getItem);
router.put('/admin/tests/:id', authed, checkPermission(ACTIONS.TESTS_UPDATE), testsController.updateItem);
router.delete('/admin/tests/:id', authed, checkPermission(ACTIONS.TESTS_DELETE), testsController.dropItem);

router.post('/admin/tests/:testId/questions', authed, checkPermission(ACTIONS.TESTS_UPDATE), testsController.createTestQuestion);
router.get('/admin/tests/:testId/questions', authed, checkPermission(ACTIONS.TESTS_READ), testsController.getTestQuestions);
router.put('/admin/tests/questions/:id', authed, checkPermission(ACTIONS.TESTS_UPDATE), testsController.saveTestQuestion);
router.put('/admin/tests/questions/:id/up', authed, checkPermission(ACTIONS.TESTS_UPDATE), testsController.moveTestQuestionUp);
router.put('/admin/tests/questions/:id/down', authed, checkPermission(ACTIONS.TESTS_UPDATE), testsController.moveTestQuestionDown);
router.delete('/admin/tests/questions/:id', authed, checkPermission(ACTIONS.TESTS_UPDATE), testsController.dropTestQuestion);

/// Test Sessions ///
router.get('/admin/testSessions', authed, checkPermission(ACTIONS.TEST_SESSIONS_READ), testSessionsController.getList);
router.post('/admin/testSessions', authed, checkPermission(ACTIONS.TEST_SESSIONS_CREATE), testSessionsController.createItem);
router.get('/admin/testSessions/:id', authed, checkPermission(ACTIONS.TEST_SESSIONS_READ), testSessionsController.getItem);
router.put('/admin/testSessions/:id', authed, checkPermission(ACTIONS.TEST_SESSIONS_UPDATE), testSessionsController.updateItem);
router.delete('/admin/testSessions/:id', authed, checkPermission(ACTIONS.TEST_SESSIONS_DELETE), testSessionsController.dropItem);

router.get('/admin/testSessions/:id/info', authed, checkPermission(ACTIONS.TEST_SESSIONS_READ), testSessionsController.getTestSessionInfo);
router.put('/admin/testSessions/:id/status', authed, checkPermission(ACTIONS.TEST_SESSIONS_READ), testSessionsController.setTestSessionStatus);
///router.get('/admin/testSessions/:id/status', authed, checkPermission(ACTIONS.TEST_SESSIONS_READ), testSessionsController.getTestSessionStatus);

/// HERE Inject Controller Routes ///


module.exports = router;
