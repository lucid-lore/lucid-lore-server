const rolesPermissions = require('../constants/rolesPermissions');

class User {
    constructor(user) {
        this.user = user;           /// todo expand nested

        this.role = user.role;
        this.schoolId = user.schoolId;
    }

    static create(user) {
        return new this(user);
    }

    getRawUser() {
        return this.user;
    }

    getId() {
        return this.user.id;
    }

    getPermissions() {
        return rolesPermissions[this.role] || [];
    }

    havePermission(permission) {
        return this.getPermissions().includes(permission);
    }

    checkPermission(permission) {                               /// FIXME refator drop
        return this.getPermissions().includes(permission);
    }

    getSchoolId() {
        return this.schoolId;
    }
}


module.exports = User;
