const testSessionsService = require('./services/testSessions');


///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

const MINUTE = 60 * 1000;

const setPeriod = (func, period = MINUTE) => {
    const handler = () => {
        const res = func();

        if (res.then) {
            res
                .finally(() => {
                    setTimeout(handler, period);
                });
        } else {
            setTimeout(handler, period);
        }
    };

    setTimeout(handler, period);
};

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

setPeriod(testSessionsService.checkUnFinishedTestSessions, 1500);       //// FIXME !!!!!
