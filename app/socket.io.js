const { EventEmitter } = require('events');
const socket_io = require('socket.io');
const authService = require('./services/auth');


const eventEmitter = new EventEmitter;

let io;
const onlineUsers = {};

const userChannel = id => 'u_' + id;
const userEventsChannel = id => 'ue_' + id;
const testSessionChannel = id => 'ts_' + id;


const initWithServer = server => {
    io = socket_io(server);

    io.on('connection', function (socket) {
        let userId;

        socket.emit('getToken');
        socket.emit('now', new Date);

        socket.on('token', async (token) => {
            const data = await authService.getSession(token);
            if (!data) return;

            userId = data.user.id;

            socket.join( userChannel(userId) );
            onlineUsers[ userId ] = socket;

            io.to( userEventsChannel(userId) ).emit('user_connected', userId);
            console.log('connected', userId);

            eventEmitter.emit('userConnected', userId);
        });

        //////////////////////////////////////////////////////////////////
        socket.emit('getSubscriptions');

        socket.on('subscribeTestSession', (testSessionId, userIds) => {
            console.log('subscribe on testSession', testSessionId, userIds);

            socket.join( testSessionChannel(testSessionId) );

            userIds.forEach(id => {                         /// FIXME maybe   CACHE_INVALIDATION
                socket.join( userEventsChannel(id) );
            });
        });
        //////////////////////////////////////////////////////////////////

        socket.on('testAppBlur', () => eventEmitter.emit('testAppBlur', userId));
        socket.on('testAppFocus', () => eventEmitter.emit('testAppFocus', userId));

        //////////////////////////////////////////////////////////////////

        socket.on('disconnect', function() {
            if (userId) {
                delete onlineUsers[userId];
                console.log('disconnected', userId);
                io.to( userEventsChannel(userId) ).emit('user_disconnected', userId);

                eventEmitter.emit('userDisconnected', userId);
            }
        });
    });
};


const getOnlineUsersByIds = (ids) => {
    return ids.filter(id => !!onlineUsers[id]);
};


const forceTestSessionUpdate = (testSessionId) => {
    io.to( testSessionChannel(testSessionId) ).emit('force_testSession_update');
};


const forceUsersToReloadTestApp = (userIds) => {
    userIds.forEach(userId => {
        io.to( userChannel(userId) ).emit('force_testApp_update');
    });
};

const forceUsersToReloadTestAppActualSessions = (userIds) => {
    userIds.forEach(userId => {
        io.to( userChannel(userId) ).emit('force_testApp_actualTestSessions_update');
    });
};


module.exports = {
    initWithServer,
    getOnlineUsersByIds,
    forceTestSessionUpdate,
    forceUsersToReloadTestApp,
    forceUsersToReloadTestAppActualSessions,
    eventEmitter
};
