const { ROLES } = require('./roles');
const ACTIONS = require('./actions');

const { ROLE_ADMIN, ROLE_SCHOOL_ADMIN, ROLE_TEACHER, ROLE_PUPIL } = ROLES;


const genMutableCrudPermissions = (readOnly = [], mutate = []) => (mutable = false) =>
    readOnly.concat(mutable && mutate).filter(v => v);


const usersAdmins = genMutableCrudPermissions(
    [ ACTIONS.USERS_ADMINS_READ ],
    [
        ACTIONS.USERS_ADMINS_CREATE,
        ACTIONS.USERS_ADMINS_UPDATE,
        ACTIONS.USERS_ADMINS_DELETE,
    ]
);

const usersSchoolAdmins = genMutableCrudPermissions(
    [ ACTIONS.USERS_SCHOOL_ADMINS_READ ],
    [
        ACTIONS.USERS_SCHOOL_ADMINS_CREATE,
        ACTIONS.USERS_SCHOOL_ADMINS_UPDATE,
        ACTIONS.USERS_SCHOOL_ADMINS_DELETE,
    ]
);

const usersTeachers = genMutableCrudPermissions(
    [ ACTIONS.USERS_TEACHERS_READ ],
    [
        ACTIONS.USERS_TEACHERS_CREATE,
        ACTIONS.USERS_TEACHERS_UPDATE,
        ACTIONS.USERS_TEACHERS_DELETE,
    ]
);

const usersPupils = genMutableCrudPermissions(
    [ ACTIONS.USERS_PUPILS_READ ],
    [
        ACTIONS.USERS_PUPILS_CREATE,
        ACTIONS.USERS_PUPILS_UPDATE,
        ACTIONS.USERS_PUPILS_DELETE,
    ]
);


const schools = genMutableCrudPermissions(
    [ ACTIONS.SCHOOLS_READ ],
    [
        ACTIONS.SCHOOLS_CREATE,
        ACTIONS.SCHOOLS_UPDATE,
        ACTIONS.SCHOOLS_DELETE,
    ]
);


const classes = genMutableCrudPermissions(
    [ ACTIONS.CLASSES_READ ],
    [
        ACTIONS.CLASSES_CREATE,
        ACTIONS.CLASSES_UPDATE,
        ACTIONS.CLASSES_DELETE,
    ]
);


const disciplines = genMutableCrudPermissions(
    [ ACTIONS.DISCIPLINES_READ ],
    [
        ACTIONS.DISCIPLINES_CREATE,
        ACTIONS.DISCIPLINES_UPDATE,
        ACTIONS.DISCIPLINES_DELETE,
    ]
);

const subjects = genMutableCrudPermissions(
    [ ACTIONS.SUBJECTS_READ ],
    [
        ACTIONS.SUBJECTS_CREATE,
        ACTIONS.SUBJECTS_UPDATE,
        ACTIONS.SUBJECTS_DELETE,
    ]
);

const testGroups = genMutableCrudPermissions(
    [ ACTIONS.TEST_GROUPS_READ ],
    [
        ACTIONS.TEST_GROUPS_CREATE,
        ACTIONS.TEST_GROUPS_UPDATE,
        ACTIONS.TEST_GROUPS_DELETE,
    ]
);


const tests = genMutableCrudPermissions(
    [ ACTIONS.TESTS_READ ],
    [
        ACTIONS.TESTS_CREATE,
        ACTIONS.TESTS_UPDATE,
        ACTIONS.TESTS_DELETE,
    ]
);


const testSessions = genMutableCrudPermissions(
    [ ACTIONS.TEST_SESSIONS_READ ],
    [
        ACTIONS.TEST_SESSIONS_CREATE,
        ACTIONS.TEST_SESSIONS_UPDATE,
        ACTIONS.TEST_SESSIONS_DELETE,
    ]
);

/// HERE Inject MutableCrudPermissions functions ///


const ROLE_PERMISSIONS = {
    [ROLE_ADMIN]: [
        ACTIONS.VIEW_ADMIN,
        ACTIONS.VIEW_DASHBOARD,

        ACTIONS.ANY_SCHOOL_ACCESS,
        ACTIONS.ANY_OWNER_ACCESS,

        ...usersAdmins(true),
        ...usersSchoolAdmins(true),
        ...usersTeachers(true),
        ...usersPupils(true),
        ACTIONS.USERS_READ,

        ...schools(true),
        ...classes(true),
        ...disciplines(true),
        ...subjects(true),

        ...testGroups(true),
        ...tests(true),
        ...testSessions(true),
        /// HERE Inject MutableCrudPermissions for ADMIN ROLE ///
    ],

    [ROLE_SCHOOL_ADMIN]: [
        ACTIONS.VIEW_ADMIN,
        ACTIONS.VIEW_DASHBOARD,

        ...usersSchoolAdmins(),
        ...usersTeachers(true),
        ...usersPupils(true),
        ACTIONS.USERS_READ,

        ...schools(),
        ACTIONS.SCHOOLS_UPDATE,

        ...classes(true),
        ...disciplines(),

        ...testGroups(true),
        ...tests(true),
        ...testSessions(true),
    ],

    [ROLE_TEACHER]: [
        ACTIONS.VIEW_ADMIN,
        ACTIONS.VIEW_DASHBOARD,

        ...usersSchoolAdmins(),
        ...usersTeachers(),
        ...usersPupils(true),
        ACTIONS.USERS_READ,

        ...schools(),
        ...classes(),
        ...disciplines(),

        ...testGroups(true),
        ...tests(true),
        ...testSessions(true),
    ],

    [ROLE_PUPIL]: []
};


module.exports = ROLE_PERMISSIONS;
