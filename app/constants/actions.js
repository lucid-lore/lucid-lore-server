const VIEW_ADMIN = 'view_admin';
const VIEW_DASHBOARD = 'view_dashboard';


/// Let admin mutate any school\user's entities
const ANY_SCHOOL_ACCESS = 'any_school_access';
const ANY_OWNER_ACCESS = 'any_owner_access';


/// SOME SEPARATED by roles USERS_PERMISSIONS
const USERS_ADMINS_READ = 'users_admins_read';
const USERS_ADMINS_CREATE = 'users_admins_create';
const USERS_ADMINS_UPDATE = 'users_admins_update';
const USERS_ADMINS_DELETE = 'users_admins_delete';

const USERS_SCHOOL_ADMINS_READ = 'users_school_admins_read';
const USERS_SCHOOL_ADMINS_CREATE = 'users_school_admins_create';
const USERS_SCHOOL_ADMINS_UPDATE = 'users_school_admins_update';
const USERS_SCHOOL_ADMINS_DELETE = 'users_school_admins_delete';

const USERS_TEACHERS_READ = 'users_teachers_read';
const USERS_TEACHERS_CREATE = 'users_teachers_create';
const USERS_TEACHERS_UPDATE = 'users_teachers_update';
const USERS_TEACHERS_DELETE = 'users_teachers_delete';

const USERS_PUPILS_READ = 'users_pupils_read';
const USERS_PUPILS_CREATE = 'users_pupils_create';
const USERS_PUPILS_UPDATE = 'users_pupils_update';
const USERS_PUPILS_DELETE = 'users_pupils_delete';

const USERS_READ = 'users_read';


const SCHOOLS_READ = 'schools_read';
const SCHOOLS_CREATE = 'schools_create';
const SCHOOLS_UPDATE = 'schools_update';
const SCHOOLS_DELETE = 'schools_delete';

const CLASSES_READ = 'classes_read';
const CLASSES_CREATE = 'classes_create';
const CLASSES_UPDATE = 'classes_update';
const CLASSES_DELETE = 'classes_delete';


const DISCIPLINES_READ = 'disciplines_read';
const DISCIPLINES_CREATE = 'disciplines_create';
const DISCIPLINES_UPDATE = 'disciplines_update';
const DISCIPLINES_DELETE = 'disciplines_delete';

const SUBJECTS_READ = 'subjects_read';
const SUBJECTS_CREATE = 'subjects_create';
const SUBJECTS_UPDATE = 'subjects_update';
const SUBJECTS_DELETE = 'subjects_delete';

const TEST_GROUPS_READ = 'test_groups_read';
const TEST_GROUPS_CREATE = 'test_groups_create';
const TEST_GROUPS_UPDATE = 'test_groups_update';
const TEST_GROUPS_DELETE = 'test_groups_delete';

const TESTS_READ = 'tests_read';
const TESTS_CREATE = 'tests_create';
const TESTS_UPDATE = 'tests_update';
const TESTS_DELETE = 'tests_delete';

const TEST_SESSIONS_READ = 'test_sessions_read';
const TEST_SESSIONS_CREATE = 'test_sessions_create';
const TEST_SESSIONS_UPDATE = 'test_sessions_update';
const TEST_SESSIONS_DELETE = 'test_sessions_delete';

/// HERE Inject Actions Definition ///


module.exports = {
    VIEW_ADMIN,
    VIEW_DASHBOARD,

    ANY_SCHOOL_ACCESS,
    ANY_OWNER_ACCESS,

    USERS_ADMINS_READ,
    USERS_ADMINS_CREATE,
    USERS_ADMINS_UPDATE,
    USERS_ADMINS_DELETE,
    USERS_SCHOOL_ADMINS_READ,
    USERS_SCHOOL_ADMINS_CREATE,
    USERS_SCHOOL_ADMINS_UPDATE,
    USERS_SCHOOL_ADMINS_DELETE,
    USERS_TEACHERS_READ,
    USERS_TEACHERS_CREATE,
    USERS_TEACHERS_UPDATE,
    USERS_TEACHERS_DELETE,
    USERS_PUPILS_READ,
    USERS_PUPILS_CREATE,
    USERS_PUPILS_UPDATE,
    USERS_PUPILS_DELETE,
    USERS_READ,

    SCHOOLS_READ,
    SCHOOLS_CREATE,
    SCHOOLS_UPDATE,
    SCHOOLS_DELETE,

    CLASSES_READ,
    CLASSES_CREATE,
    CLASSES_UPDATE,
    CLASSES_DELETE,

    DISCIPLINES_READ,
    DISCIPLINES_CREATE,
    DISCIPLINES_UPDATE,
    DISCIPLINES_DELETE,

    SUBJECTS_READ,
    SUBJECTS_CREATE,
    SUBJECTS_UPDATE,
    SUBJECTS_DELETE,

    TEST_GROUPS_READ,
    TEST_GROUPS_CREATE,
    TEST_GROUPS_UPDATE,
    TEST_GROUPS_DELETE,

    TESTS_READ,
    TESTS_CREATE,
    TESTS_UPDATE,
    TESTS_DELETE,

    TEST_SESSIONS_READ,
    TEST_SESSIONS_CREATE,
    TEST_SESSIONS_UPDATE,
    TEST_SESSIONS_DELETE,

    /// HERE Inject Actions Export ///
};
