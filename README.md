@see PREPARE_DB.md

To use Knex CLI options its recommended to install knex globally first

    npm install knex -g

# Preparаtion
    npm i
    npm i -g pm2
    
    cp config/local{.example,}.js       ## OR       cp config/local{.prod,}.js
    cp knexfile{.example,}.js
     

# Start dev through pm2
    pm2 start dev.config.js

# Start prod through pm2
    pm2 start prod.config.js





# Migrations
    knex migrate:list
    knex migrate:make migration_name
    knex migrate:up
    knex migrate:down
    knex migrate:latest
    knex migrate:rollback
    knex migrate:rollback --all

# Seeds
    knex seed:make seed_name
    knex seed:run
    knex seed:run --specific=seed-filename.js




## Project structure

    /app - All Application code is here
        /constants - Some static data                                           -- Domain Specific
        /controllers - Request handlers                                         -- Domain Specific Handlers Logic & Adapters
        /middlewares - Request payload handlers                                 -- Adapters with bit of domain specific
        /models - Data models (DB layer)                                        -- Domain Specific Primitives to interact with data layer
        /providers - External services
        /services - High Level Logic                                            -- Domain Specific
        /utils  -   Non-project specific useful things
        app.js - Express Application
        routes.js - Express Routes
    /bin - Executable scripts
        www - App's web server listening
    /config - TODO
    /db  -   app domain details                                                 -- Domain Specific
        /migrations
        /seeds
    /logs
    dev.config.js - pm2 config file for dev server
    knexfile.js - DB connections
    PREPARE_DB.md - instructions to create db
    README.md - this



### Special environment config
Конфиг импортится через `config/index.js` и состоит из объединения `config/default.js` и `config.local.js`.

Локальный конфиг находится в гитигноре. Для него существует пример для разработки `config/index.example.js`, и так же конфиги под разные площадки типа `config/local.prod.js`.



## Code Generation
    
    npm i -g hygen
    
    hygen crudEntity new %someNameEntities%