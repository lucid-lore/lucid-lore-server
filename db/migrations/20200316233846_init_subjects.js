
const TABLE = 'subjects';

exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.increments('id');
        table.text('name').notNullable().unique();

        table.integer('discipline_id')
            .notNullable()
            .references('id').inTable('disciplines');

        table.integer('parent_id')
            .references('id').inTable(TABLE);

        table.integer('order')
            .notNullable();

    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};
