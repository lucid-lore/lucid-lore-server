
const TABLE = 'disciplines';

exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.increments('id');
        table.text('name').notNullable().unique();
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};
