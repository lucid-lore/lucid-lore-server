
const TABLE = 'test_group_users';

exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.integer('test_group_id')
            .notNullable()
            .references('id').inTable('test_groups');

        table.integer('user_id')
            .notNullable()
            .references('id').inTable('users');
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};
