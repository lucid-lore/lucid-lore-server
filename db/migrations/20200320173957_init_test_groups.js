
const TABLE = 'test_groups';

exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.increments('id');
        table.text('name').notNullable();

        table.integer('school_id')
            .notNullable()
            .references('id').inTable('schools');

        table.integer('owner_id')
            .notNullable()
            .references('id').inTable('users');
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};
