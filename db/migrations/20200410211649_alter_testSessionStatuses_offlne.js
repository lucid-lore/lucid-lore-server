
const TABLE = 'test_session_statuses';

exports.up = async function(knex) {
    await knex.schema.alterTable(TABLE, table => {
        table.dateTime('started_at', { useTz: true, precision: 6 });
        table.dateTime('finished_at', { useTz: true, precision: 6 });

        table.dateTime('offline_at', { useTz: true, precision: 6 });

        table.integer('offline_duration')
            .notNullable()
            .defaultTo(0)
            .comment('secs');

        table.integer('blurred_duration')
            .notNullable()
            .defaultTo(0)
            .alter();
    });
};


exports.down = async function(knex) {
    await knex.schema.alterTable(TABLE, table => {
        table.dropColumn('started_at');
        table.dropColumn('finished_at');
        table.dropColumn('offline_at');
        table.dropColumn('offline_duration');
    });
};
