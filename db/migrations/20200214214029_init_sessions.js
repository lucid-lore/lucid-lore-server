/* CREATE TABLE IF NOT EXISTS sessions (
    id serial primary key,
    user_id int references users(id) unique,
    session_key text,
    sk_creation_time timestamptz NOT NULL DEFAULT (CURRENT_TIMESTAMP at time zone 'UTC')
); */

/* CREATE TABLE "api_session" (
    "sid" varchar NOT NULL COLLATE "default",
    "sess" json NOT NULL,
    "expire" timestamp(6) NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE "api_session" ADD CONSTRAINT "api_session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE; */


exports.up = async function(knex) {
    await knex.schema.createTable('sessions', table => {
        table.text('sid').primary();

        table.integer('user_id').unique()
            .notNullable()
            .references('id').inTable('users');

        table.timestamp('touched', { useTz: true, precision: 6 })
            .notNullable()
            .defaultTo( knex.fn.now() );
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable('sessions');
};
