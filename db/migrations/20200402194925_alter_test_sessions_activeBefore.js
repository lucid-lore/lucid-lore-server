
const TABLE = 'test_sessions';

exports.up = async function(knex) {
    await knex.schema.alterTable(TABLE, table => {
        table.dateTime('active_before', { useTz: true, precision: 6 });
    });
};


exports.down = async function(knex) {
    await knex.schema.alterTable(TABLE, table => {
        table.dropColumn('active_before');
    });
};
