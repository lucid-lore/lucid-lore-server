
const TABLE = 'classes';

exports.up = async function(knex) {
    await knex.schema.table(TABLE, function (table) {
        table.dropColumn('comment');
    });
};

exports.down = async function(knex) {
    await knex.schema.table(TABLE, function (table) {
        table.text('comment');
    });
};
