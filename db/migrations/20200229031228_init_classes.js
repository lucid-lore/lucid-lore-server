
const TABLE = 'classes';


exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.increments('id');

        table.integer('school_id')
            .notNullable()
            .references('id').inTable('schools');

        table.integer('level');

        table.text('letter');

        table.text('name');

        table.text('comment');
    });
};


exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};

