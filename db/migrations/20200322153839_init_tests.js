
const TABLE = 'tests';

exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.increments('id');
        table.text('name').notNullable();

        table.integer('discipline_id')
            .notNullable()
            .references('id').inTable('disciplines');

        table.integer('owner_id')
            .notNullable()
            .references('id').inTable('users');

        table.jsonb('datas')
            .notNullable()
            .defaultTo({});
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};
