
exports.up = async function(knex) {
    await knex.schema.table('users', function (table) {
        table.integer('school_id')
            .references('id').inTable('schools');

        table.integer('class_id')
            .references('id').inTable('classes');

        table.text('fio');

        table.date('birthday');

        table.text('email');
        table.text('phone');

        table.text('comment');
    });
};

exports.down = async function(knex) {
    await knex.schema.table('users', function (table) {
        table.dropColumn('school_id');
        table.dropColumn('class_id');
        table.dropColumn('fio');
        table.dropColumn('birthday');
        table.dropColumn('email');
        table.dropColumn('phone');
        table.dropColumn('comment');
    });
};
