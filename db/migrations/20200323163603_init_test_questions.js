
const TABLE = 'test_questions';

exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.increments('id');

        table.integer('test_id')
            .notNullable()
            .references('id').inTable('tests');

        table.enum('type', [ 'options', 'input' ])
            .defaultTo('options');

        table.integer('order');

        table.text('text');

        table.specificType('answers', 'text[]')
            .defaultTo('{}');

        table.text('right_answer');
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};
