
const TABLE = 'test_sessions';

exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.increments('id');

        table.integer('owner_id')
            .notNullable()
            .references('id').inTable('users');

        table.integer('test_id')
            .notNullable()
            .references('id').inTable('tests');

        table.integer('test_group_id')
            .notNullable()
            .references('id').inTable('test_groups');

        table.text('name')
            .notNullable();

        table.integer('duration')
            .notNullable();

        table.dateTime('preparing_at', { useTz: true, precision: 6 });
        table.dateTime('started_at', { useTz: true, precision: 6 });

        table.enum('status', [ 'draft', 'planned', 'preparing', 'active', 'finished' ])
            .notNullable()
            .defaultTo('draft');
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};
