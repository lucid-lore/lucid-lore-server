
const TABLE = 'test_answers';

exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.increments('id');

        table.integer('user_id')
            .notNullable()
            .references('id').inTable('users');

        table.integer('test_session_id')
            .notNullable()
            .references('id').inTable('test_sessions');

        table.integer('test_question_id')
            .notNullable()
            .references('id').inTable('test_questions');

        table.text('answer')
            .notNullable();

        table.dateTime('created_at', { useTz: true, precision: 6 })
            .notNullable()
            .defaultTo( knex.fn.now() );

        table.unique([ 'user_id', 'test_session_id', 'test_question_id' ]);
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};
