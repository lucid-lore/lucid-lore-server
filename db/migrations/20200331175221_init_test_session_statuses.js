
const TABLE = 'test_session_statuses';

exports.up = async function(knex) {
    await knex.schema.createTable(TABLE, table => {
        table.increments('id');

        table.integer('user_id')
            .notNullable()
            .references('id').inTable('users');

        table.integer('test_session_id')
            .notNullable()
            .references('id').inTable('test_sessions');

        table.boolean('finished')
            .notNullable()
            .defaultTo(false);

        table.integer('question_number')
            .defaultTo(1);

        table.dateTime('blurred_at', { useTz: true, precision: 6 });

        table.integer('blurred_duration')
            .comment('secs');

        table.unique([ 'user_id', 'test_session_id' ]);
    });
};

exports.down = async function(knex) {
    await knex.schema.dropTable(TABLE);
};
