# Initial
    sudo -u postgres psql

    \l
    \du
    
    ### !!! FIXME password on prod !!! ###
    CREATE DATABASE lucid_lore;
    CREATE USER lucid_lore WITH ENCRYPTED PASSWORD 'qweqwe';        
    GRANT ALL PRIVILEGES ON DATABASE lucid_lore TO lucid_lore;
    
    ## For tests
    
    CREATE DATABASE lucid_lore_test;
    GRANT ALL PRIVILEGES ON DATABASE lucid_lore_test TO lucid_lore;